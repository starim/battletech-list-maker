# frozen_string_literal: true

# == Schema Information
#
# Table name: faction_categories
#
#  id   :integer          not null, primary key
#  name :text             not null
#

FactoryBot.define do
  factory :faction_category do
    sequence(:name) { |n| "Faction Category #{n}" }
  end
end
