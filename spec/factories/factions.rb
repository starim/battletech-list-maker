# frozen_string_literal: true

# == Schema Information
#
# Table name: factions
#
#  id                  :integer          not null, primary key
#  name                :text             not null
#  faction_category_id :integer          not null
#

FactoryBot.define do
  factory :faction do
    sequence(:name) { |n| "Faction #{n}" }
    faction_category
  end
end
