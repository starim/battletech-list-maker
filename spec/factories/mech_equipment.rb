# frozen_string_literal: true

# == Schema Information
#
# Table name: mech_equipment
#
#  id              :bigint           not null, primary key
#  equipment_id    :bigint           not null
#  mech_variant_id :bigint           not null
#  count           :integer          not null
#  rear_facing     :boolean          default(FALSE), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

FactoryBot.define do
  factory :mech_equipment do
    mech_variant
    equipment
    count { 1 }
  end
end
