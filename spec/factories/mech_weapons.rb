# frozen_string_literal: true

# == Schema Information
#
# Table name: mech_weapons
#
#  id              :bigint           not null, primary key
#  weapon_id       :bigint           not null
#  mech_variant_id :bigint           not null
#  count           :integer          not null
#  rear_facing     :boolean          default(FALSE), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

FactoryBot.define do
  factory :mech_weapon do
    mech_variant
    weapon
    count { 1 }
    rear_facing { false }
  end
end
