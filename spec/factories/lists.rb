# == Schema Information
#
# Table name: lists
#
#  name                :string           not null
#  target_battle_value :integer          not null
#  era_id              :bigint
#  faction_category_id :bigint
#  rules_level_id      :bigint
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  id                  :uuid             not null, primary key
#
FactoryBot.define do
  factory :list do
    sequence(:name) { |n| "List ##{n}" }
    target_battle_value { 5_000 }

    trait :with_list_entry do
      after(:build) do |list, evaluator|
        list.list_entries << build(:list_entry, list: list)
      end
    end
  end
end
