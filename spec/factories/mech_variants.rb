# frozen_string_literal: true

# == Schema Information
#
# Table name: mech_variants
#
#  id                 :integer          not null, primary key
#  tonnage            :integer          not null
#  base_battle_value  :integer          not null
#  cbill_cost         :integer
#  rules_level_id     :integer          not null
#  year_introduced    :integer          not null
#  mul_url            :text             not null
#  heat_sinks         :string           not null
#  walking_mp         :integer          not null
#  jumping_mp         :integer          not null
#  front_armor        :integer          not null
#  rear_armor         :integer          not null
#  engine_model       :string           not null
#  structure_type     :string           not null
#  armor_type         :string           not null
#  mech_chassis_id    :integer          not null
#  omnimech           :boolean          default(FALSE), not null
#  designation        :string           not null
#  technology_base_id :bigint           not null
#  role_id            :bigint
#  full_name          :text             not null
#

FactoryBot.define do
  factory :mech_variant do
    sequence(:full_name) { |n| "Mech Variant #{n}" }
    sequence(:designation) { |n| "Mech Variant #{n}" }
    tonnage { 20 }
    base_battle_value { 1 }
    cbill_cost { nil }
    rules_level
    year_introduced { 2500 }
    sequence(:mul_url) { |n| "http://example.com/#{n}" }
    omnimech { false }
    association :chassis, factory: :mech_chassis
    heat_sinks { "10 single" }
    walking_mp { 4 }
    jumping_mp { 3 }
    front_armor { 20 }
    rear_armor { 15 }
    engine_model { "180 fusion engine" }
    structure_type { "Endo-Steel" }
    armor_type { "Ferro-Fibrous" }
    technology_base
  end
end
