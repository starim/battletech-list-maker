# frozen_string_literal: true

# == Schema Information
#
# Table name: technology_bases
#
#  id   :integer          not null, primary key
#  name :text             not null
#

FactoryBot.define do
  factory :technology_base do
    sequence(:name) { |n| "Tech Base #{n}" }
  end
end
