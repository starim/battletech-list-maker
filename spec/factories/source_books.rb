# frozen_string_literal: true

# == Schema Information
#
# Table name: source_books
#
#  id   :integer          not null, primary key
#  name :text             not null
#

FactoryBot.define do
  factory :source_book do
    sequence(:name) { |n| "Source Book #{n}" }
  end
end
