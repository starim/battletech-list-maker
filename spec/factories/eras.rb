# frozen_string_literal: true

# == Schema Information
#
# Table name: eras
#
#  id          :integer          not null, primary key
#  name        :text             not null
#  start_year  :integer          not null
#  end_year    :integer          not null
#  description :text
#

FactoryBot.define do
  factory :era do
    sequence(:name) { |n| "Era #{n}" }
    start_year { 0 }
    end_year { 0 }
    description { "description goes here" }
  end
end
