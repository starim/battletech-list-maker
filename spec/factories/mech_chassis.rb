# frozen_string_literal: true

# == Schema Information
#
# Table name: mech_chassis
#
#  id                         :bigint           not null, primary key
#  name                       :string           not null
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  clan_name                  :string
#  plastic_model_release_date :date
#  plastic_model_owned        :boolean          default(FALSE), not null
#  sarna_url                  :string           not null
#

FactoryBot.define do
  factory :mech_chassis do
    sequence(:name) { |n| "Mech Chassis #{n}" }
    sarna_url { "https://www.example.com/mech" }

    after :build do |chassis|
      path = "#{Rails.root}/spec/fixtures/files/image.png"
      chassis.image.attach(Rack::Test::UploadedFile.new(path, "image/png"))
    end

    trait :with_mech_variant do
      mech_variant
    end
  end
end
