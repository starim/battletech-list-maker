# frozen_string_literal: true

# == Schema Information
#
# Table name: availability
#
#  id              :integer          not null, primary key
#  era_id          :integer          not null
#  mech_variant_id :integer          not null
#  faction_id      :integer          not null
#

FactoryBot.define do
  factory :availability do
    era
    mech_variant
    faction
  end
end
