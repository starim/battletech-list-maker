# == Schema Information
#
# Table name: list_entries
#
#  id              :bigint           not null, primary key
#  battle_value_id :bigint           not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  list_id         :uuid             not null
#
FactoryBot.define do
  factory :list_entry do
    list
    battle_value
  end
end
