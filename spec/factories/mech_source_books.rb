# frozen_string_literal: true

# == Schema Information
#
# Table name: mech_source_books
#
#  id              :integer          not null, primary key
#  mech_variant_id :integer          not null
#  source_book_id  :integer          not null
#

FactoryBot.define do
  factory :mech_source_book do
    mech_variant
    source_book
  end
end
