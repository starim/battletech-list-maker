# frozen_string_literal: true

# == Schema Information
#
# Table name: rules_levels
#
#  id               :integer          not null, primary key
#  name             :text             not null
#  tournament_legal :boolean          not null
#  level            :integer          not null
#  description      :text             not null
#

FactoryBot.define do
  factory :rules_level do
    sequence(:name) { |n| "Rules Level #{n}" }
    tournament_legal { true }
    sequence(:level)
    description { "A rules level." }
  end
end
