# frozen_string_literal: true

# == Schema Information
#
# Table name: equipment
#
#  id         :bigint           not null, primary key
#  name       :string           not null
#  weapon     :boolean          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :equipment do
    sequence(:name) { |n| "Equipment #{n}" }
    weapon { true }
  end
end
