# frozen_string_literal: true

# == Schema Information
#
# Table name: battle_values
#
#  id              :integer          not null, primary key
#  mech_variant_id :integer          not null
#  piloting_skill  :integer          not null
#  gunnery_skill   :integer          not null
#  battle_value    :integer          not null
#

FactoryBot.define do
  factory :battle_value do
    mech_variant
    piloting_skill { 0 }
    gunnery_skill { 0 }
    battle_value { 1 }
  end
end
