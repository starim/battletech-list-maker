# == Schema Information
#
# Table name: lists
#
#  name                :string           not null
#  target_battle_value :integer          not null
#  era_id              :bigint
#  faction_category_id :bigint
#  rules_level_id      :bigint
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  id                  :uuid             not null, primary key
#
require 'rails_helper'

RSpec.describe List, type: :model do
  describe "validations" do
    subject { build(:list) }

    describe "for name" do
      it "requires its presence" do
        subject.name = nil
        expect(subject).to be_invalid
        expect(subject.errors[:name]).not_to be_empty
      end
    end

    describe "for target battle value" do
      it "require its presence" do
        subject.target_battle_value = nil
        expect(subject).to be_invalid
        expect(subject.errors[:target_battle_value]).not_to be_empty
      end

      it "require that it be non-negative" do
        subject.target_battle_value = -1
        expect(subject).to be_invalid
        expect(subject.errors[:target_battle_value]).not_to be_empty
      end

      it "require that it be non-zero" do
        subject.target_battle_value = 0
        expect(subject).to be_invalid
        expect(subject.errors[:target_battle_value]).not_to be_empty
      end
    end

    describe "for uniqueness" do
      let!(:original) { create(:list) }

      it "should ensure name is unique" do
        subject.name = original.name
        expect(subject).to be_invalid
        expect(subject.errors[:name]).not_to be_empty
      end
    end

    describe "for default piloting skill" do
      it "require its presence" do
        subject.default_piloting_skill = nil
        expect(subject).to be_invalid
        expect(subject.errors[:default_piloting_skill]).not_to be_empty
      end

      it "require that it be non-negative" do
        subject.default_piloting_skill = -1
        expect(subject).to be_invalid
        expect(subject.errors[:default_piloting_skill]).not_to be_empty
      end
    end

    describe "for default gunnery skill" do
      it "require its presence" do
        subject.default_gunnery_skill = nil
        expect(subject).to be_invalid
        expect(subject.errors[:default_gunnery_skill]).not_to be_empty
      end

      it "require that it be non-negative" do
        subject.default_gunnery_skill = -1
        expect(subject).to be_invalid
        expect(subject.errors[:default_gunnery_skill]).not_to be_empty
      end
    end
  end
end
