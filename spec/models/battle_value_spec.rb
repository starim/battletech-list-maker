# frozen_string_literal: true

# == Schema Information
#
# Table name: battle_values
#
#  id              :integer          not null, primary key
#  mech_variant_id :integer          not null
#  piloting_skill  :integer          not null
#  gunnery_skill   :integer          not null
#  battle_value    :integer          not null
#

require "rails_helper"

RSpec.describe BattleValue, type: :model do
  describe "validations" do
    subject { build(:battle_value) }

    describe "for piloting skill" do
      it "require its presence" do
        subject.piloting_skill = nil
        expect(subject).to be_invalid
        expect(subject.errors[:piloting_skill]).not_to be_empty
      end

      it "require it be an integer" do
        subject.piloting_skill = 4.33
        expect(subject).to be_invalid
        expect(subject.errors[:piloting_skill]).not_to be_empty
      end

      it "require that it can't exceed MAX_SKILL_VALUE" do
        subject.piloting_skill = Rails.application.config.MAX_SKILL_VALUE + 1
        expect(subject).to be_invalid
        expect(subject.errors[:piloting_skill]).not_to be_empty
      end

      it "require that it can't go below MIN_SKILL_VALUE" do
        subject.piloting_skill = Rails.application.config.MIN_SKILL_VALUE - 1
        expect(subject).to be_invalid
        expect(subject.errors[:piloting_skill]).not_to be_empty
      end
    end

    describe "for gunnery skill" do
      it "require its presence" do
        subject.gunnery_skill = nil
        expect(subject).to be_invalid
        expect(subject.errors[:gunnery_skill]).not_to be_empty
      end

      it "require it be an integer" do
        subject.gunnery_skill = 4.33
        expect(subject).to be_invalid
        expect(subject.errors[:gunnery_skill]).not_to be_empty
      end

      it "require that it can't exceed MAX_SKILL_VALUE" do
        subject.gunnery_skill = Rails.application.config.MAX_SKILL_VALUE + 1
        expect(subject).to be_invalid
        expect(subject.errors[:gunnery_skill]).not_to be_empty
      end

      it "require that it can't go below MIN_SKILL_VALUE" do
        subject.gunnery_skill = Rails.application.config.MIN_SKILL_VALUE - 1
        expect(subject).to be_invalid
        expect(subject.errors[:gunnery_skill]).not_to be_empty
      end
    end

    describe "for battle value" do
      it "require its presence" do
        subject.battle_value = nil
        expect(subject).to be_invalid
        expect(subject.errors[:battle_value]).not_to be_empty
      end

      it "require that it be non-negative" do
        subject.battle_value = -1
        expect(subject).to be_invalid
        expect(subject.errors[:battle_value]).not_to be_empty
      end

      it "require that it be non-zero" do
        subject.battle_value = 0
        expect(subject).to be_invalid
        expect(subject.errors[:battle_value]).not_to be_empty
      end
    end

    describe "for uniqueness" do
      let!(:original) { create(:battle_value) }

      describe "of associated mech variant" do
        context "when the same mech is associated with the same piloting skill and gunnery skill" do
          it "is invalid" do
            subject.mech_variant = original.mech_variant
            subject.piloting_skill = original.piloting_skill
            subject.gunnery_skill = original.gunnery_skill
            expect(subject).to be_invalid
            expect(subject.errors[:mech_variant_id]).not_to be_empty
          end
        end

        context "when the same mech is associated with the same piloting skill but a different gunnery skill" do
          it "is valid" do
            subject.mech_variant = original.mech_variant
            subject.piloting_skill = original.piloting_skill
            subject.gunnery_skill = original.gunnery_skill + 1
            expect(subject).to be_valid
          end
        end

        context "when the same mech is associated with the same gunnery skill but a different piloting skill" do
          it "is valid" do
            subject.mech_variant = original.mech_variant
            subject.piloting_skill = original.piloting_skill + 1
            subject.gunnery_skill = original.gunnery_skill
            expect(subject).to be_valid
          end
        end
      end
    end
  end
end
