# frozen_string_literal: true

# == Schema Information
#
# Table name: rules_levels
#
#  id               :integer          not null, primary key
#  name             :text             not null
#  tournament_legal :boolean          not null
#  level            :integer          not null
#  description      :text             not null
#

require "rails_helper"

RSpec.describe RulesLevel, type: :model do
  describe "validations" do
    subject { build(:rules_level) }

    it "check for presence of name" do
      subject.name = nil
      expect(subject).to be_invalid
      expect(subject.errors[:name]).not_to be_empty
    end

    it "check for presence of level" do
      subject.level = nil
      expect(subject).to be_invalid
      expect(subject.errors[:level]).not_to be_empty
    end

    describe "for uniqueness" do
      let!(:original) { create(:rules_level) }

      it "should ensure name is unique" do
        subject.name = original.name
        expect(subject).to be_invalid
        expect(subject.errors[:name]).not_to be_empty
      end

      it "should ensure level is unique" do
        subject.level = original.level
        expect(subject).to be_invalid
        expect(subject.errors[:level]).not_to be_empty
      end
    end
  end
end
