# frozen_string_literal: true

# == Schema Information
#
# Table name: mech_variants
#
#  id                 :integer          not null, primary key
#  tonnage            :integer          not null
#  base_battle_value  :integer          not null
#  cbill_cost         :integer
#  rules_level_id     :integer          not null
#  year_introduced    :integer          not null
#  mul_url            :text             not null
#  heat_sinks         :string           not null
#  walking_mp         :integer          not null
#  jumping_mp         :integer          not null
#  front_armor        :integer          not null
#  rear_armor         :integer          not null
#  engine_model       :string           not null
#  structure_type     :string           not null
#  armor_type         :string           not null
#  mech_chassis_id    :integer          not null
#  omnimech           :boolean          default(FALSE), not null
#  designation        :string           not null
#  technology_base_id :bigint           not null
#  role_id            :bigint
#  full_name          :text             not null
#

require "rails_helper"

RSpec.describe MechVariant, type: :model do
  describe "validations" do
    subject { build(:mech_variant) }

    describe "for full_name" do
      it "require its presence" do
        subject.full_name = nil
        expect(subject).to be_invalid
        expect(subject.errors[:full_name]).not_to be_empty
      end
    end

    describe "for tonnage" do
      it "require its presence" do
        subject.tonnage = nil
        expect(subject).to be_invalid
        expect(subject.errors[:tonnage]).not_to be_empty
      end

      it "require that it be non-negative" do
        subject.tonnage = -1
        expect(subject).to be_invalid
        expect(subject.errors[:tonnage]).not_to be_empty
      end
    end

    describe "for base battle value" do
      it "require its presence" do
        subject.base_battle_value = nil
        expect(subject).to be_invalid
        expect(subject.errors[:base_battle_value]).not_to be_empty
      end

      it "require that it be non-negative" do
        subject.base_battle_value = -1
        expect(subject).to be_invalid
        expect(subject.errors[:base_battle_value]).not_to be_empty
      end

      it "require that it be non-zero" do
        subject.base_battle_value = 0
        expect(subject).to be_invalid
        expect(subject.errors[:base_battle_value]).not_to be_empty
      end
    end

    describe "for CBill cost" do
      it "require that it be non-negative" do
        subject.cbill_cost = -1
        expect(subject).to be_invalid
        expect(subject.errors[:cbill_cost]).not_to be_empty
      end

      it "require that it be non-zero" do
        subject.cbill_cost = 0
        expect(subject).to be_invalid
        expect(subject.errors[:cbill_cost]).not_to be_empty
      end
    end

    describe "for heat sinks" do
      it "require its presence" do
        subject.heat_sinks = nil
        expect(subject).to be_invalid
        expect(subject.errors[:heat_sinks]).not_to be_empty
      end
    end

    describe "for walking MP" do
      it "require its presence" do
        subject.walking_mp = nil
        expect(subject).to be_invalid
        expect(subject.errors[:walking_mp]).not_to be_empty
      end

      it "require that it be non-negative" do
        subject.walking_mp = -1
        expect(subject).to be_invalid
        expect(subject.errors[:walking_mp]).not_to be_empty
      end
    end

    describe "for jumping MP" do
      it "require its presence" do
        subject.jumping_mp = nil
        expect(subject).to be_invalid
        expect(subject.errors[:jumping_mp]).not_to be_empty
      end

      it "require that it be non-negative" do
        subject.jumping_mp = -1
        expect(subject).to be_invalid
        expect(subject.errors[:jumping_mp]).not_to be_empty
      end
    end

    describe "for front_armor" do
      it "require its presence" do
        subject.front_armor = nil
        expect(subject).to be_invalid
        expect(subject.errors[:front_armor]).not_to be_empty
      end

      it "require that it be non-negative" do
        subject.front_armor = -1
        expect(subject).to be_invalid
        expect(subject.errors[:front_armor]).not_to be_empty
      end
    end

    describe "for rear_armor" do
      it "require its presence" do
        subject.rear_armor = nil
        expect(subject).to be_invalid
        expect(subject.errors[:rear_armor]).not_to be_empty
      end

      it "require that it be non-negative" do
        subject.rear_armor = -1
        expect(subject).to be_invalid
        expect(subject.errors[:rear_armor]).not_to be_empty
      end
    end

    describe "for engine model" do
      it "require its presence" do
        subject.engine_model = nil
        expect(subject).to be_invalid
        expect(subject.errors[:engine_model]).not_to be_empty
      end
    end

    describe "for structure type" do
      it "require its presence" do
        subject.structure_type = nil
        expect(subject).to be_invalid
        expect(subject.errors[:structure_type]).not_to be_empty
      end
    end

    describe "for armor type" do
      it "require its presence" do
        subject.armor_type = nil
        expect(subject).to be_invalid
        expect(subject.errors[:armor_type]).not_to be_empty
      end
    end

    describe "for year introduced" do
      it "require its presence" do
        subject.year_introduced = nil
        expect(subject).to be_invalid
        expect(subject.errors[:year_introduced]).not_to be_empty
      end

      it "require it be an integer" do
        subject.year_introduced = 4.2
        expect(subject).to be_invalid
        expect(subject.errors[:year_introduced]).not_to be_empty
      end
    end

    describe "for Master Unit List URL" do
      it "require its presence" do
        subject.mul_url = nil
        expect(subject).to be_invalid
        expect(subject.errors[:mul_url]).not_to be_empty
      end
    end

    describe "for uniqueness" do
      let!(:original) { create(:mech_variant) }

      it "should ensure full_name is unique" do
        subject.full_name = original.full_name
        expect(subject).to be_invalid
        expect(subject.errors[:full_name]).not_to be_empty
      end

      describe "of designation" do
        context "when the same designation is associated with the same chassis" do
          it "is invalid" do
            subject.designation = original.designation
            subject.chassis = original.chassis
            expect(subject).to be_invalid
            expect(subject.errors[:designation]).not_to be_empty
          end
        end

        context "when the same designation is associated with different chassis" do
          it "is valid" do
            subject.designation = original.designation
            expect(subject).to be_valid
          end
        end
      end

      it "should ensure mul_url is unique" do
        subject.mul_url = original.mul_url
        expect(subject).to be_invalid
        expect(subject.errors[:mul_url]).not_to be_empty
      end
    end
  end

  describe "scopes" do
    describe "by_full_name" do
      let!(:matched_mech) { create :mech_variant, full_name: "~match~" }
      let!(:matched_mech_case_insensitive) do
        create :mech_variant, full_name: "~MATCH~"
      end
      let!(:unmatched_mech) { create :mech_variant, full_name: "not" }

      subject { MechVariant.by_full_name("match").to_a }

      it do
        is_expected.to match_array([
          matched_mech, matched_mech_case_insensitive
        ])
      end
    end

    describe "by_role" do
      let(:role) { create :role }

      let!(:matched_mech) { create :mech_variant, role: role }
      let!(:unmatched_mech) { create :mech_variant }

      subject { MechVariant.by_role(role).to_a }

      it do
        is_expected.to eq([matched_mech])
      end
    end

    describe "from_era" do
      let(:era) { create :era }

      let!(:matched_mech) do
        mech = create :mech_variant
        create :availability, mech_variant: mech, era: era
        mech.reload
      end
      let!(:unmatched_mech) { create :mech_variant }

      subject { MechVariant.from_era(era).to_a }

      it do
        is_expected.to eq([matched_mech])
      end
    end

    describe "from_faction_category" do
      let(:faction_category) { create :faction_category }

      let!(:matched_mech) do
        faction = create :faction, faction_category: faction_category
        mech = create :mech_variant
        create :availability, mech_variant: mech, faction: faction
        mech.reload
      end
      let!(:unmatched_mech) { create :mech_variant }

      subject { MechVariant.from_faction_category(faction_category).to_a }

      it do
        is_expected.to eq([matched_mech])
      end
    end

    describe "from_faction_category" do
      let(:faction) { create :faction }

      let!(:matched_mech) do
        mech = create :mech_variant
        create :availability, mech_variant: mech, faction: faction
        mech.reload
      end
      let!(:unmatched_mech) { create :mech_variant }

      subject { MechVariant.from_faction(faction).to_a }

      it do
        is_expected.to eq([matched_mech])
      end
    end

    describe "legal_in_rules_level" do
      let!(:rules_level1) { create :rules_level, level: 1 }
      let!(:rules_level2) { create :rules_level, level: 2 }
      let!(:rules_level3) { create :rules_level, level: 3 }

      let!(:mech1) { create(:mech_variant, rules_level: rules_level1) }
      let!(:mech2) { create(:mech_variant, rules_level: rules_level2) }
      let!(:mech3) { create(:mech_variant, rules_level: rules_level3) }

      subject { MechVariant.legal_in_rules_level(2).to_a }

      it { is_expected.to match_array([mech1, mech2]) }
    end
  end

end
