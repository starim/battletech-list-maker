# frozen_string_literal: true

# == Schema Information
#
# Table name: mech_equipment
#
#  id              :bigint           not null, primary key
#  equipment_id    :bigint           not null
#  mech_variant_id :bigint           not null
#  count           :integer          not null
#  rear_facing     :boolean          default(FALSE), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

require "rails_helper"

RSpec.describe MechEquipment, type: :model do
  describe "validations" do
    subject { build(:mech_equipment) }

    describe "for count" do
      it "require its presence" do
        subject.count = nil
        expect(subject).to be_invalid
        expect(subject.errors[:count]).not_to be_empty
      end

      it "require that it be non-negative" do
        subject.count = -1
        expect(subject).to be_invalid
        expect(subject.errors[:count]).not_to be_empty
      end

      it "require that it be non-zero" do
        subject.count = 0
        expect(subject).to be_invalid
        expect(subject.errors[:count]).not_to be_empty
      end
    end

    describe "for uniqueness" do
      let!(:original) { create(:mech_equipment) }

      describe "of associated mech variant" do
        context "when the same equipment with the same facing is associated with the same mech variant" do
          it "is invalid" do
            subject.mech_variant = original.mech_variant
            subject.equipment = original.equipment
            subject.rear_facing = original.rear_facing
            expect(subject).to be_invalid
            expect(subject.errors[:mech_variant_id]).not_to be_empty
          end
        end

        context "when the same equipment with the same facing is associated with a different mech variant" do
          it "is valid" do
            subject.equipment = original.equipment
            subject.rear_facing = original.rear_facing
            expect(subject).to be_valid
          end
        end

        context "when the same equipment with a different facing is associated with the same mech variant" do
          it "is valid" do
            subject.mech_variant = original.mech_variant
            subject.equipment = original.equipment
            subject.rear_facing = !original.rear_facing
            expect(subject).to be_valid
          end
        end
      end
    end
  end
end
