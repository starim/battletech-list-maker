# frozen_string_literal: true

# == Schema Information
#
# Table name: eras
#
#  id          :integer          not null, primary key
#  name        :text             not null
#  start_year  :integer          not null
#  end_year    :integer          not null
#  description :text
#

require "rails_helper"

RSpec.describe Era, type: :model do
  describe "validations" do
    subject { build(:era) }

    describe "for name" do
      it "require its presence" do
        subject.name = nil
        expect(subject).to be_invalid
        expect(subject.errors[:name]).not_to be_empty
      end
    end

    describe "for start year" do
      it "require its presence" do
        subject.start_year = nil
        expect(subject).to be_invalid
        expect(subject.errors[:start_year]).not_to be_empty
      end

      it "require that it be an integer" do
        subject.start_year = 4.33
        expect(subject).to be_invalid
        expect(subject.errors[:start_year]).not_to be_empty
      end
    end

    describe "for end year" do
      it "require its presence" do
        subject.end_year = nil
        expect(subject).to be_invalid
        expect(subject.errors[:end_year]).not_to be_empty
      end

      it "require that it be an integer" do
        subject.end_year = 4.33
        expect(subject).to be_invalid
        expect(subject.errors[:end_year]).not_to be_empty
      end
    end

    describe "for start_year and end_year" do
      context "when start_year is before end_year" do
        subject { build(:era, start_year: 1999, end_year: 2000) }

        it "is valid" do
          expect(subject.valid?).to eq(true)
        end
      end

      context "when start_year is equal to end_year" do
        subject { build(:era, start_year: 1999, end_year: 1999) }

        it "is valid" do
          expect(subject.valid?).to eq(true)
        end
      end

      context "when start_year is after end_year" do
        subject { build(:era, start_year: 1999, end_year: 1998) }

        it "isn't valid" do
          expect(subject.valid?).to eq(false)
        end

        it "attaches an error to end_year" do
          subject.valid?
          expect(subject.errors[:end_year]).not_to be_empty
        end
      end
    end

    describe "for uniqueness" do
      let!(:original) { create(:era) }

      it "should ensure name is unique" do
        subject.name = original.name
        expect(subject).to be_invalid
        expect(subject.errors[:name]).not_to be_empty
      end
    end
  end
end
