# frozen_string_literal: true

# == Schema Information
#
# Table name: faction_categories
#
#  id   :integer          not null, primary key
#  name :text             not null
#

require "rails_helper"

RSpec.describe FactionCategory, type: :model do
  describe "validations" do
    subject { build(:faction_category) }

    describe "for name" do
      it "require its presence" do
        subject.name = nil
        expect(subject).to be_invalid
        expect(subject.errors[:name]).not_to be_empty
      end
    end

    describe "for uniqueness" do
      let!(:original) { create(:faction_category) }

      it "should ensure name is unique" do
        subject.name = original.name
        expect(subject).to be_invalid
        expect(subject.errors[:name]).not_to be_empty
      end
    end
  end
end
