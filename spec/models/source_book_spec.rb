# frozen_string_literal: true

# == Schema Information
#
# Table name: source_books
#
#  id   :integer          not null, primary key
#  name :text             not null
#

require "rails_helper"

RSpec.describe SourceBook, type: :model do
  describe "validations" do
    subject { build(:source_book) }

    it "check for presence of name" do
      subject.name = nil
      expect(subject).to be_invalid
      expect(subject.errors[:name]).not_to be_empty
    end

    describe "for uniqueness" do
      let!(:original) { create(:source_book) }

      it "should ensure name is unique" do
        subject.name = original.name
        expect(subject).to be_invalid
        expect(subject.errors[:name]).not_to be_empty
      end
    end
  end
end
