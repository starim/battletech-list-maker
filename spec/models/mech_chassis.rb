# frozen_string_literal: true

require "rails_helper"

RSpec.describe MechChassis, type: :model do
  describe "validations" do
    subject { build(:mech_chassis) }

    describe "for name" do
      it "require its presence" do
        subject.name = nil
        expect(subject).to be_invalid
        expect(subject.errors[:name]).not_to be_empty
      end
    end

    describe "for sarna_url" do
      it "require its presence" do
        subject.sarna_url = nil
        expect(subject).to be_invalid
        expect(subject.errors[:sarna_url]).not_to be_empty
      end
    end

    describe "for uniqueness" do
      let!(:original) { create(:mech_chassis, clan_name: "Gladiator") }

      describe "of name" do
        context "when the same name is reused with the same clan name" do
          it "is invalid" do
            subject.name = original.name
            subject.clan_name = original.clan_name
            expect(subject).to be_invalid
            expect(subject.errors[:name]).not_to be_empty
          end
        end

        context "when the same name is reused with a different clan name" do
          it "is valid" do
            subject.name = original.name
            expect(subject).to be_valid
          end
        end
      end
    end
  end

  describe "scopes" do
    describe "by_model_availability" do
      let!(:owned_chassis) do
        create(
          :mech_chassis,
          plastic_model_owned: true,
          plastic_model_release_date: 1.year.ago,
        )
      end
      let!(:preordered_chassis) do
        create(
          :mech_chassis,
          plastic_model_owned: true,
          plastic_model_release_date: 1.month.from_now,
        )
      end
      let!(:chassis_without_plastic_model) do
        create(
          :mech_chassis,
          plastic_model_owned: false,
          plastic_model_release_date: nil,
        )
      end
      let!(:unowned_chassis) do
        create(
          :mech_chassis,
          plastic_model_owned: false,
          plastic_model_release_date: 1.year.ago,
        )
      end

      subject { MechChassis.by_model_availability(filter).to_a }

      context "when owned" do
        let(:filter) { "owned" }

        it { is_expected.to eq([owned_chassis]) }
      end

      context "when owned&pre-ordered" do
        let(:filter) { "owned&pre-ordered" }

        it { is_expected.to match_array([owned_chassis, preordered_chassis]) }
      end

      context "when a different model availability string is passed" do
        let(:filter) { "no-model" }

        it "raises an error" do
          expect { subject }.to raise_error(<<~ERROR_MESSAGE.squish)
            Unrecognized model_availability parameter value to
            MechChassis.by_model_availability: no-model
          ERROR_MESSAGE
        end
      end
    end
  end
end
