# frozen_string_literal: true

# == Schema Information
#
# Table name: roles
#
#  id         :bigint           not null, primary key
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require "rails_helper"

RSpec.describe Role, type: :model do
  describe "validations" do
    subject { build(:role) }

    it "check for presence of name" do
      subject.name = nil
      expect(subject).to be_invalid
      expect(subject.errors[:name]).not_to be_empty
    end

    describe "for uniqueness" do
      let!(:original) { create(:role) }

      it "should ensure name is unique" do
        subject.name = original.name
        expect(subject).to be_invalid
        expect(subject.errors[:name]).not_to be_empty
      end
    end
  end
end
