# frozen_string_literal: true

# == Schema Information
#
# Table name: mech_source_books
#
#  id              :integer          not null, primary key
#  mech_variant_id :integer          not null
#  source_book_id  :integer          not null
#

require "rails_helper"

RSpec.describe MechSourceBook, type: :model do
  describe "validations" do
    subject { build(:mech_source_book) }

    describe "for uniqueness" do
      let!(:original) { create(:mech_source_book) }

      describe "of mech variant" do
        context "when the same mech variant is associated with the same source book" do
          it "is invalid" do
            subject.mech_variant_id = original.mech_variant_id
            subject.source_book = original.source_book
            expect(subject).to be_invalid
            expect(subject.errors[:mech_variant_id]).not_to be_empty
          end
        end

        context "when the same mech variant is associated with a different source book" do
          it "is valid" do
            subject.mech_variant_id = original.mech_variant_id
            expect(subject).to be_valid
          end
        end
      end
    end
  end
end
