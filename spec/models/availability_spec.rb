# frozen_string_literal: true

# == Schema Information
#
# Table name: availability
#
#  id              :integer          not null, primary key
#  era_id          :integer          not null
#  mech_variant_id :integer          not null
#  faction_id      :integer          not null
#

require "rails_helper"

RSpec.describe Availability, type: :model do
  describe "validations" do
    describe "for uniqueness" do
      describe "on mech_variant_id" do
        subject { create(:availability) }

        it "should enforce uniqueness for the same faction and era" do
          duplicate = FactoryBot.build(
            :availability,
            mech_variant: subject.mech_variant,
            era: subject.era,
            faction: subject.faction,
          )
          expect(duplicate.valid?).to eq(false)
          expect(duplicate.errors).to have_key(:mech_variant_id)
        end

        it "shouldn't enforce uniqueness for a different faction" do
          subject = FactoryBot.create :availability
          duplicate = FactoryBot.build(
            :availability,
            mech_variant: subject.mech_variant,
            era: subject.era,
          )
          expect(duplicate.valid?).to eq(true)
        end

        it "shouldn't enforce uniqueness for a different era" do
          subject = FactoryBot.create :availability
          duplicate = FactoryBot.build(
            :availability,
            mech_variant: subject.mech_variant,
            faction: subject.faction,
          )
          expect(duplicate.valid?).to eq(true)
        end
      end
    end
  end
end
