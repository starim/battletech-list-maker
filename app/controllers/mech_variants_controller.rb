class MechVariantsController < ApplicationController
  def index
    @list = List.find(params[:list_id])
    @mech_chassis = MechChassis.find(params[:mech_chassis_id])
    mech_chassis_id_filter = @mech_chassis.id
    @chassis_page = page_param(params, :chassis_page)

    era_id_filter = @list.era_id if @list.era
    faction_category_id_filter = @list.faction_category_id if @list.faction_category_id
    rules_level_filter = @list.rules_level_id if @list.rules_level_id

    @mechs = MechVariant.distinct.where(mech_chassis_id: @mech_chassis)

    if session[:role_id_filter].present?
      @mechs = @mechs.by_role(session[:role_id_filter])
    end
    if era_id_filter.present?
      @mechs = @mechs.from_era(era_id_filter)
    end
    if faction_category_id_filter.present?
      @mechs =
        @mechs.from_faction_category(faction_category_id_filter)
    end
    if session[:faction_id_filter].present?
      @mechs = @mechs.from_faction(session[:faction_id_filter])
    end
    if rules_level_filter.present?
      @mechs =
        @mechs.legal_in_rules_level(rules_level_filter)
    end

    @mech_variants =
      @mechs
      .includes([:chassis, :role, :battle_values])
      .joins(:chassis)
      .select("mech_variants.*, mech_chassis.name")
      .order(Arel.sql(<<~ORDER_BY))
        mech_variants.tonnage ASC, mech_variants.year_introduced ASC,
        mech_variants.designation ASC
      ORDER_BY
  rescue => error
    @mech_variants = []
    Rails.logger.error error
    # TODO: display error
  end
end
