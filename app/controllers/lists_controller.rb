class ListsController < ApplicationController
  def index
    @lists = List.includes(:era, :faction_category, :rules_level).order(updated_at: :desc).limit(10)
  end

  def new
    @list = List.new

    # set defaults
    @list.faction_category = FactionCategory.where(name: "Inner Sphere").first
    @list.rules_level = RulesLevel.where(name: "Advanced").first
  end

  def create
    @list = List.new(list_params)

    respond_to do |format|
      if @list.save
        format.html { redirect_to list_url(@list) }
      else
        flash.now[:alert] = @list.errors.full_messages
        format.html { render :new }
        format.turbo_stream { render turbo_stream: turbo_stream.replace(@list, partial: "lists/list_form", locals: { list: @list }) }
      end
    end
  end

  def edit
    @list = List.find(params[:id])

    if @list.nil?
      head :not_found
      return
    end
  end

  def update
    @list =
      List
      .includes(:list_entries, :battle_values, :mech_variants)
      .find_by(id: params[:id])

    if @list.nil?
      head :not_found
      return
    end

    @list.assign_attributes(list_params.except(:list_entries))
    if list_params["list_entries"]
      set_list_entries(@list, list_params["list_entries"])
    end

    respond_to do |format|
      if @list.save
        format.html { redirect_to list_url(@list) }
      else
        flash.now[:alert] = @list.errors.full_messages
        format.html { render edit_list_url(@list), status: :unprocessable_entity }
        format.turbo_stream { render turbo_stream: turbo_stream.replace(@list, partial: "lists/list_form", locals: { list: @list }) }
      end
    end
  end

  def show
    @list =
      List
      .includes(
        :list_entries, :battle_values, :mech_variants, :era, :faction_category,
        :rules_level
      )
      .find_by(id: params[:id])
  end

  private

    def list_params
      params.require(:list).permit([
        :name,
        :target_battle_value,
        :era_id,
        :faction_category_id,
        :rules_level_id,
        :default_gunnery_skill,
        :default_piloting_skill,
        list_entries: [
          :id,
          :mech_variant_id,
          :piloting_skill,
          :gunnery_skill,
        ],
      ])
    end

    def set_list_entries(list, list_entry_params)
      destroy_removed_list_entries(list, list_entry_params)
      update_list_entries(list, list_entry_params)
      add_new_list_entries(list, list_entry_params)
    end

    def destroy_removed_list_entries(list, list_entry_params)
      ids_to_keep = list_entry_params.filter_map {|entry| entry["id"] }
      list
        .list_entries
        .filter {|list_entry| !ids_to_keep.include?(list_entry.id) }
        .each {|list_entry| list_entry.destroy }
    end

    def update_list_entries(list, list_entry_params)
      list_entry_params
        .find_all {|entry_params| entry_params["id"] }
        .map do |entry_params|
          list_entry =
            list
            .list_entries
            .find {|list_entry| list_entry.id == entry_params["id"] }

          battle_value = BattleValue.find_by(
            mech_variant: MechVariant.find_by(id: entry_params["mech_variant_id"]),
            piloting_skill: entry_params["piloting_skill"].to_i,
            gunnery_skill: entry_params["gunnery_skill"].to_i,
          )
          list_entry.battle_value = battle_value
        end
    end

    def add_new_list_entries(list, list_entry_params)
      list_entry_params
        .find_all {|entry_params| !entry_params["id"] }
        .map do |entry_params|
          battle_value = BattleValue.find_by(
            mech_variant: MechVariant.find_by(id: entry_params["mech_variant_id"]),
            piloting_skill: entry_params["piloting_skill"].to_i,
            gunnery_skill: entry_params["gunnery_skill"].to_i,
          )
          ListEntry.new(list: list, battle_value: battle_value)
        end
        .each {|list_entry|
          list.list_entries << list_entry
        }
    end
end
