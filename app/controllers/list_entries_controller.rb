class ListEntriesController < ApplicationController
  def index
  end

  def new
    @list = List.find(params[:list_id])
    @page = page_param(params)
    page_size = Rails.configuration.pagination_page_size

    era_id_filter = @list.era_id if @list.era
    faction_category_id_filter = @list.faction_category_id if @list.faction_category
    rules_level_filter = @list.rules_level_id if @list.rules_level

    @faction_options_for_faction_filter = [Faction.new] + Faction.where(
      faction_category: @list.faction_category,
      availability: { era: @list.era },
    ).joins(availability: :era).distinct.order(id: :asc).all.to_a

    if params[:full_name_filter]
      session[:full_name_filter] = params[:full_name_filter]
    end
    if params[:role_id_filter]
      session[:role_id_filter] = params[:role_id_filter]
    end

    if params[:faction_id_filter]
      session[:faction_id_filter] = params[:faction_id_filter]
    end
    if params[:model_availability_filter]
      session[:model_availability_filter] = params[:model_availability_filter]
    end

    mech_variant_scopes = MechVariant
    at_least_one_mech_variant_filter = false

    if session[:full_name_filter].present?
      at_least_one_mech_variant_filter = true
      mech_variant_scopes =
        mech_variant_scopes.by_full_name(session[:full_name_filter])
    end
    if session[:role_id_filter].present?
      at_least_one_mech_variant_filter = true
      mech_variant_scopes =
        mech_variant_scopes.by_role(session[:role_id_filter])
    end
    if era_id_filter.present?
      at_least_one_mech_variant_filter = true
      mech_variant_scopes =
        mech_variant_scopes.from_era(era_id_filter)
    end
    if faction_category_id_filter.present?
      at_least_one_mech_variant_filter = true
      mech_variant_scopes =
        mech_variant_scopes.from_faction_category(faction_category_id_filter)
    end
    if session[:faction_id_filter].present?
      at_least_one_mech_variant_filter = true
      mech_variant_scopes =
        mech_variant_scopes.from_faction(session[:faction_id_filter])
    end
    if rules_level_filter.present?
      at_least_one_mech_variant_filter = true
      mech_variant_scopes =
        mech_variant_scopes.legal_in_rules_level(rules_level_filter)
    end

    subquery = MechChassis.joins(:mech_variants)
    if at_least_one_mech_variant_filter
      subquery = subquery.merge(mech_variant_scopes)
    end

    if session[:model_availability_filter].present?
      subquery = subquery.by_model_availability(session[:model_availability_filter])
    end

    @chassis_on_this_page =
      MechChassis
      .includes(:mech_variants)
      .joins(:mech_variants)
      .order(tonnage: :asc, name: :asc)
      .where(id:
        subquery
        .group(:id)
        .offset((@page - 1) * page_size)
        .limit(page_size)
        .order("MIN(mech_variants.tonnage) ASC, mech_chassis.name ASC")
      )

    @total_chassis = subquery.distinct(:id).count
    @total_pages = (@total_chassis / page_size.to_f).ceil
    Rails.logger.info "Found #{@total_chassis} matching mech chassis"
  rescue => error
    @chassis_on_this_page = []
    @total_chassis = 0
    @total_pages = 0
    Rails.logger.error error
    # TODO: display error
  end

  def create
    @list_entry = ListEntry.new(allowed_create_params)
    @list = @list_entry.list
    respond_to do |format|
      if @list_entry.save
        format.html { redirect_to @list }
        format.turbo_stream
      else
        format.turbo_stream { render turbo_stream: turbo_stream.replace("new-list-entry-flash", partial: "new_list_entry_flash") }
      end
    end
  end

  def edit
    @list_entry = ListEntry.find(params[:id])
    @type = params[:type] || "gunnery"
    @battle_values = @list_entry.mech_variant.battle_values
    if @type == "gunnery"
      @battle_values = @battle_values.where(piloting_skill: @list_entry.battle_value.piloting_skill).order(gunnery_skill: :desc)
    else
      @battle_values = @battle_values.where(gunnery_skill: @list_entry.battle_value.gunnery_skill).order(piloting_skill: :desc)
    end

    respond_to do |format|
      format.html { render :edit }
      format.turbo_stream { render turbo_stream: turbo_stream.replace("list-entry-card-#{@list_entry.id}", :edit) }
    end
  end

  def update
    @list_entry =
      ListEntry
      .includes(:battle_value, :mech_variant)
      .find_by(id: params[:id])
    @list = @list_entry.list

    if @list_entry.nil?
      head :not_found
      return
    end

    @list_entry.assign_attributes(list_entry_update_params)

    if @list_entry.save
      respond_to do |format|
        format.html { redirect_to list_entry_url(@list_entry) }
        format.turbo_stream
      end
    else
      flash.now[:alert] = @list_entry.errors.full_messages
      respond_to do |format|
        format.html { render edit_list_entry_url(@list_entry), status: :unprocessable_entity }
        format.turbo_stream { render turbo_stream: turbo_stream.replace("list-entry-card-#{@list_entry.id}", :edit) }
      end
    end
  end

  def show
    @list_entry = ListEntry.find(params[:id])
  end

  def destroy
    @list_entry = ListEntry.find(params[:id])
    @list = @list_entry.list
    @list_entry.destroy
    respond_to do |format|
      format.html { redirect_to @list }
      format.turbo_stream
    end
  end

  private

    def allowed_create_params
      params.require(:list_entry).permit(:list_id, :battle_value_id)
    end

    def list_entry_update_params
      params.require(:list_entry).permit([:battle_value_id])
    end
end
