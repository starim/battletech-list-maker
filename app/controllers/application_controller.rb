class ApplicationController < ActionController::Base
  def page_param(params, param_name = :page)
    begin
      page = Integer(params[param_name], 10)
    rescue TypeError
      page = 1
    rescue ArgumentError
      page = 1
    end

    if page < 1
      page = 1
    end

    page
  end
end
