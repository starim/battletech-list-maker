# == Schema Information
#
# Table name: battle_values
#
#  id              :integer          not null, primary key
#  mech_variant_id :integer          not null
#  piloting_skill  :integer          not null
#  gunnery_skill   :integer          not null
#  battle_value    :integer          not null
#

class BattleValue < ApplicationRecord
  validates :piloting_skill, :gunnery_skill, :battle_value, presence: true

  validates :piloting_skill, :gunnery_skill, numericality: {
    only_integer: true,
    greater_than_or_equal_to: Rails.application.config.MIN_SKILL_VALUE,
    less_than_or_equal_to: Rails.application.config.MAX_SKILL_VALUE,
  }
  validates :battle_value, numericality: { only_integer: true, greater_than: 0 }

  validates(
    :mech_variant_id,
    uniqueness: { scope: %i[piloting_skill gunnery_skill] }
  )

  belongs_to :mech_variant, inverse_of: :battle_values
  has_many :list_entries, inverse_of: :battle_value, dependent:
    :restrict_with_error
  has_many :lists, through: :list_entries, inverse_of: :battle_values
end
