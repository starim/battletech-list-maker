# == Schema Information
#
# Table name: list_entries
#
#  id              :bigint           not null, primary key
#  battle_value_id :bigint           not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  list_id         :uuid             not null
#
class ListEntry < ApplicationRecord
  belongs_to :list, inverse_of: :list_entries
  belongs_to :battle_value, inverse_of: :list_entries
  has_one :mech_variant, through: :battle_value, inverse_of: :list_entries
end
