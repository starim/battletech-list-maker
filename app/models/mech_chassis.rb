# == Schema Information
#
# Table name: mech_chassis
#
#  id                         :bigint           not null, primary key
#  name                       :string           not null
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  clan_name                  :string
#  plastic_model_release_date :date
#  plastic_model_owned        :boolean          default(FALSE), not null
#  sarna_url                  :string           not null
#

class MechChassis < ApplicationRecord
  validates :name, :sarna_url, presence: true
  validates :name, uniqueness: { scope: [:clan_name] }
  validate :require_image, :require_image_be_an_image_file

  def require_image
    if !image.attached?
      errors.add(:image, "is required")
    end
  end

  def require_image_be_an_image_file
    if image.attached?
      if !image.image?
        errors.add(:image, "must be an image file")
      end
    end
  end

  has_many(
    :mech_variants,
    inverse_of: :chassis,
    dependent: :restrict_with_exception,
  )

  has_one_attached :image

  # scopes

  def self.by_model_availability(model_availability)
    case model_availability
    when "plastic_model_owned"
      where(plastic_model_owned: true)
    when "plastic_model_exists"
      where("plastic_model_release_date IS NOT NULL")
    else
      raise <<~ERROR_MESSAGE.squish
        Unrecognized model_availability parameter value to
        MechChassis.by_model_availability: #{model_availability}
      ERROR_MESSAGE
    end
  end
end
