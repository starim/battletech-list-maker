# == Schema Information
#
# Table name: factions
#
#  id                  :integer          not null, primary key
#  name                :text             not null
#  faction_category_id :integer          not null
#

class Faction < ApplicationRecord
  validates :name, presence: true
  validates :name, uniqueness: true

  belongs_to :faction_category, optional: false, inverse_of: :factions
  has_many(
    :availability,
    inverse_of: :faction,
    dependent: :restrict_with_exception,
  )
  has_many :eras, through: :availability, inverse_of: :factions
  has_many :mech_variants, through: :availability, inverse_of: :factions
end
