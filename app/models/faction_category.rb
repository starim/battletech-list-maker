# == Schema Information
#
# Table name: faction_categories
#
#  id   :integer          not null, primary key
#  name :text             not null
#

class FactionCategory < ApplicationRecord
  validates :name, presence: true
  validates :name, uniqueness: true

  has_many :factions, inverse_of: :faction_category, dependent:
    :restrict_with_error
  has_many :lists, inverse_of: :faction_category, dependent:
    :restrict_with_error

  scope :with_era_ids, lambda {
    select(<<~SQL)
      faction_categories.*,
      (SELECT array(
        SELECT DISTINCT availability.era_id
        FROM availability
        INNER JOIN factions ON factions.id=availability.faction_id
        WHERE factions.faction_category_id=faction_categories.id
      )) AS era_ids
    SQL
  }
end
