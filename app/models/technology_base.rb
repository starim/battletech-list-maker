# == Schema Information
#
# Table name: technology_bases
#
#  id   :integer          not null, primary key
#  name :text             not null
#

class TechnologyBase < ApplicationRecord
  validates :name, presence: true
  validates :name, uniqueness: true

  has_many(
    :mech_variants,
    inverse_of: :technology_base,
    dependent: :restrict_with_exception,
  )
end
