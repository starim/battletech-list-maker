# == Schema Information
#
# Table name: eras
#
#  id          :integer          not null, primary key
#  name        :text             not null
#  start_year  :integer          not null
#  end_year    :integer          not null
#  description :text
#

class Era < ApplicationRecord
  validates :name, :start_year, :end_year, :description, presence: true
  validates :name, uniqueness: true

  validates :start_year, :end_year, numericality: { only_integer: true }
  validate :start_year_before_end_year

  has_many :availability, inverse_of: :era, dependent: :restrict_with_error
  has_many :mech_variants, through: :availability, inverse_of: :eras
  has_many :factions, through: :availability, inverse_of: :eras
  has_many :lists, inverse_of: :era, dependent: :restrict_with_error

  private

  def start_year_before_end_year
    if start_year && end_year && start_year > end_year
      errors.add(:end_year, "cannot be before start_year")
    end
  end
end
