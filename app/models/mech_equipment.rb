# == Schema Information
#
# Table name: mech_equipment
#
#  id              :bigint           not null, primary key
#  equipment_id    :bigint           not null
#  mech_variant_id :bigint           not null
#  count           :integer          not null
#  rear_facing     :boolean          default(FALSE), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class MechEquipment < ApplicationRecord
  validates :count, numericality: { only_integer: true, greater_than: 0 }
  validates(
    :mech_variant_id,
    uniqueness: { scope: %i[equipment_id rear_facing] },
  )

  belongs_to :mech_variant, optional: false, inverse_of: :mech_equipment
  belongs_to :equipment, optional: false, inverse_of: :mech_equipment
end
