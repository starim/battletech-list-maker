# == Schema Information
#
# Table name: mech_variants
#
#  id                 :integer          not null, primary key
#  tonnage            :integer          not null
#  base_battle_value  :integer          not null
#  cbill_cost         :integer
#  rules_level_id     :integer          not null
#  year_introduced    :integer          not null
#  mul_url            :text             not null
#  heat_sinks         :string           not null
#  walking_mp         :integer          not null
#  jumping_mp         :integer          not null
#  front_armor        :integer          not null
#  rear_armor         :integer          not null
#  engine_model       :string           not null
#  structure_type     :string           not null
#  armor_type         :string           not null
#  mech_chassis_id    :integer          not null
#  omnimech           :boolean          default(FALSE), not null
#  designation        :string           not null
#  technology_base_id :bigint           not null
#  role_id            :bigint
#  full_name          :text             not null
#

class MechVariant < ApplicationRecord
  validates :full_name, :designation, :tonnage, :base_battle_value,
    :year_introduced, :mul_url, :heat_sinks, :walking_mp, :jumping_mp,
    :front_armor, :rear_armor, :engine_model, :structure_type, :armor_type,
    presence: true
  validates :full_name, :mul_url, uniqueness: true
  validates :designation, uniqueness: { scope: [:mech_chassis_id] }
  validates :tonnage, :base_battle_value, numericality:
    { only_integer: true, greater_than: 0 }
  validates :cbill_cost, numericality:
    { only_integer: true, greater_than: 0, allow_nil: true }
  validates :year_introduced, numericality: { only_integer: true }
  validates :walking_mp, :jumping_mp, :front_armor, :rear_armor, numericality:
    { only_integer: true, greater_than_or_equal_to: 0 }

  belongs_to :chassis, class_name: "MechChassis", foreign_key:
    :mech_chassis_id, inverse_of: :mech_variants
  belongs_to :rules_level, optional: false, inverse_of: :mech_variants
  belongs_to :technology_base, optional: false, inverse_of: :mech_variants
  belongs_to :role, optional: true, inverse_of: :mech_variants

  has_many :mech_source_books, inverse_of: :mech_variant, dependent: :destroy
  has_many(
    :source_books,
    through: :mech_source_books,
    inverse_of: :mech_variants,
  )
  has_many :availability, inverse_of: :mech_variant, dependent: :destroy
  has_many :eras, through: :availability, inverse_of: :mech_variants
  has_many :factions, through: :availability, inverse_of: :mech_variants
  has_many :battle_values, inverse_of: :mech_variant, dependent: :destroy
  has_many :mech_equipment, inverse_of: :mech_variant, dependent: :destroy
  has_many :equipment, through: :mech_equipment, inverse_of: :mech_variants
  has_many :list_entries, through: :battle_values, inverse_of: :mech_variant
  has_many :lists, through: :list_entries, inverse_of: :mech_variants

  def total_armor
    front_armor + rear_armor
  end

  # scopes

  def self.by_full_name(full_name)
    where("mech_variants.full_name ILIKE ?", "%#{full_name}%")
  end

  def self.by_role(role_id)
    where(role_id: role_id)
  end

  def self.from_era(era_id)
    joins(:availability)
    .where(availability: { era_id: era_id })
  end

  def self.from_faction_category(faction_category_id)
    joins([availability: [:faction]])
    .where({
      availability: {
        factions: { faction_category_id: faction_category_id },
      },
    })
  end

  def self.from_faction(faction_id)
    joins(:availability)
    .where(availability: { faction_id: faction_id })
  end

  def self.legal_in_rules_level(level)
    joins(:rules_level)
      .where(rules_levels: { level: ..level })
  end

  # end scopes

  def weapons
    mech_equipment
      .joins(:equipment)
      .includes(:equipment)
      .where(equipment: { weapon: true })
      .order('equipment.name ASC')
  end

  def non_weapon_equipment
    mech_equipment
      .joins(:equipment)
      .includes(:equipment)
      .where(equipment: { weapon: false })
      .order('equipment.name ASC')
  end
end
