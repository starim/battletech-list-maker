# == Schema Information
#
# Table name: lists
#
#  name                :string           not null
#  target_battle_value :integer          not null
#  era_id              :bigint
#  faction_category_id :bigint
#  rules_level_id      :bigint
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  id                  :uuid             not null, primary key
#
class List < ApplicationRecord
  validates :name, :target_battle_value, :default_piloting_skill, :default_gunnery_skill,
    presence: true
  validates :name, uniqueness: true
  validates :target_battle_value,
    numericality: { only_integer: true, greater_than: 0 }
  validates :default_piloting_skill, :default_gunnery_skill,
    numericality: { only_integer: true, greater_than_or_equal:
                    Rails.configuration.MIN_SKILL_VALUE, less_than_or_equal:
                    Rails.configuration.MAX_SKILL_VALUE }

  belongs_to :era, required: false, inverse_of: :lists
  belongs_to :faction_category, required: false, inverse_of: :lists
  belongs_to :rules_level, required: false, inverse_of: :lists

  has_many :list_entries, inverse_of: :list, dependent: :destroy
  has_many :battle_values, through: :list_entries, inverse_of: :lists
  has_many :mech_variants, through: :battle_values, inverse_of: :lists
end
