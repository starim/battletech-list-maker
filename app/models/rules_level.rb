# == Schema Information
#
# Table name: rules_levels
#
#  id               :integer          not null, primary key
#  name             :text             not null
#  tournament_legal :boolean          not null
#  level            :integer          not null
#  description      :text             not null
#

class RulesLevel < ApplicationRecord
  validates :name, :level, :description, presence: true
  validates :name, :level, uniqueness: true
  validates :level, numericality: { only_integer: true, greater_than: 0 }

  has_many :mech_variants, inverse_of: :rules_level, dependent:
    :restrict_with_error
  has_many :lists, inverse_of: :rules_level, dependent: :restrict_with_error
end
