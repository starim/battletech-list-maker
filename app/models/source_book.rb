# == Schema Information
#
# Table name: source_books
#
#  id   :integer          not null, primary key
#  name :text             not null
#

class SourceBook < ApplicationRecord
  validates :name, presence: true
  validates :name, uniqueness: true

  has_many(
    :mech_source_books,
    inverse_of: :source_book,
    dependent: :restrict_with_exception,
  )
  has_many(
    :mech_variants,
    through: :mech_source_books,
    inverse_of: :source_books,
    dependent: :restrict_with_exception,
  )
end
