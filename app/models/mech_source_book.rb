# == Schema Information
#
# Table name: mech_source_books
#
#  id              :integer          not null, primary key
#  mech_variant_id :integer          not null
#  source_book_id  :integer          not null
#

class MechSourceBook < ApplicationRecord
  validates :mech_variant_id, uniqueness: { scope: :source_book_id }

  belongs_to :mech_variant, optional: false, inverse_of: :mech_source_books
  belongs_to :source_book, optional: false, inverse_of: :mech_source_books
end
