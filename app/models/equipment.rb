# == Schema Information
#
# Table name: equipment
#
#  id         :bigint           not null, primary key
#  name       :string           not null
#  weapon     :boolean          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Equipment < ApplicationRecord
  validates :name, presence: true
  validates :name, uniqueness: true

  has_many(
    :mech_equipment,
    inverse_of: :equipment,
    dependent: :restrict_with_exception,
  )
  has_many :mech_variants, through: :mech_equipment, inverse_of: :equipment
end
