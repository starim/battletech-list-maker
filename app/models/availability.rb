# == Schema Information
#
# Table name: availability
#
#  id              :integer          not null, primary key
#  era_id          :integer          not null
#  mech_variant_id :integer          not null
#  faction_id      :integer          not null
#

class Availability < ApplicationRecord
  self.table_name = "availability"

  validates :mech_variant_id, uniqueness: { scope: %i[faction_id era_id] }

  belongs_to :era, optional: false, inverse_of: :availability
  belongs_to :mech_variant, optional: false, inverse_of: :availability
  belongs_to :faction, optional: false, inverse_of: :availability
end
