# == Schema Information
#
# Table name: roles
#
#  id         :bigint           not null, primary key
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Role < ApplicationRecord
  validates :name, presence: true
  validates :name, uniqueness: true

  has_many(
    :mech_variants,
    inverse_of: :role,
    dependent: :restrict_with_exception,
  )
end
