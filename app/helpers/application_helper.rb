module ApplicationHelper
  def image_filename_for_era(era_name)
    if era_name.include? "Succession War"
      # Early and Late Succession Wars eras use the same image
      "succession_wars.png"
    else
      "#{era_name.parameterize.underscore}.png"
    end
  end

  def battle_value_for_mech_variant(mech_variant, gunnery_skill, piloting_skill)
    mech_variant.battle_values.find do |battle_value|
      battle_value.piloting_skill == piloting_skill &&
        battle_value.gunnery_skill == gunnery_skill
    end
  end

  def battle_value_remaining(list)
    current_battle_value = list.battle_values.map do |battle_value|
      battle_value.battle_value
    end.sum(0)
    list.target_battle_value - current_battle_value
  end

  def weapon_list_for_mech(mech_variant)
    mech_variant.weapons.map do |weapon|
      text = weapon.equipment.name
      if weapon.rear_facing
        text += " (R)"
      end
      if weapon.count > 1
        text += " x#{weapon.count}"
      end
      text
    end
  end

  def non_weapon_equipment_list_for_mech(mech_variant)
    mech_variant.non_weapon_equipment.map do |mech_equipment|
      mech_equipment.equipment.name
    end
  end
end
