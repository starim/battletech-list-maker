# frozen_string_literal: true

class AddFullNameToMechVariant < ActiveRecord::Migration[6.0]
  def up
    change_table :mech_variants, bulk: true do |t|
      t.text :full_name
    end

    MechVariant.reset_column_information
    MechVariant.includes(:chassis).find_each do |mech|
      mech.full_name = "#{mech.chassis.name} #{mech.designation}"
      mech.save!
    end

    change_table :mech_variants, bulk: true do |t|
      t.change :full_name, :text, null: false
    end
    add_index :mech_variants, :full_name, unique: true
  end

  def down
    change_table :mech_variants, bulk: true do |t|
      t.remove :full_name
    end
  end

  class MechVariant < ApplicationRecord
    validates :designation, :tonnage, :base_battle_value, :year_introduced,
      :mul_url, :heat_sinks, :walking_mp, :jumping_mp, :front_armor, :rear_armor,
      :engine_model, :structure_type, :armor_type, presence: true
    validates :mul_url, uniqueness: true
    validates :designation, uniqueness: { scope: [:mech_chassis_id] }
    validates :tonnage, :base_battle_value, numericality:
      { only_integer: true, greater_than: 0 }
    validates :cbill_cost, numericality:
      { only_integer: true, greater_than: 0, allow_nil: true }
    validates :year_introduced, numericality: { only_integer: true }
    validates :walking_mp, :jumping_mp, :front_armor, :rear_armor, numericality:
      { only_integer: true, greater_than_or_equal_to: 0 }

    belongs_to :chassis, class_name: "MechChassis", foreign_key:
      :mech_chassis_id, inverse_of: :mech_variants
    belongs_to :rules_level, optional: false, inverse_of: :mech_variants
    belongs_to :technology_base, optional: false, inverse_of: :mech_variants
    belongs_to :role, optional: true, inverse_of: :mech_variants

    has_many :mech_source_books, inverse_of: :mech_variant, dependent: :destroy
    has_many :source_books, through: :mech_source_books, inverse_of: :mech_variants
    has_many :availability, inverse_of: :mech_variant, dependent: :destroy
    has_many :eras, through: :availability, inverse_of: :mech_variants
    has_many :battle_values, inverse_of: :mech_variant, dependent: :destroy
    has_many :mech_equipment, inverse_of: :mech_variant, dependent: :destroy
    has_many :equipment, through: :mech_equipment, inverse_of: :mech_variants
  end
end
