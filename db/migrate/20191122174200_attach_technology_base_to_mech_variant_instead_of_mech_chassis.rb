# frozen_string_literal: true

class AttachTechnologyBaseToMechVariantInsteadOfMechChassis < ActiveRecord::Migration[6.0]
  def up
    add_reference :mech_variants, :technology_base, null: false, foreign_key: true
    remove_column :mech_chassis, :technology_base_id
  end

  def down
    add_reference :mech_chassis, :technology_base, null: false, foreign_key: true
    remove_column :mech_variants, :technology_base_id
  end
end
