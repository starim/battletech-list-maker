# frozen_string_literal: true

class CreateHelmCoreData < ActiveRecord::Migration[6.0]
  def change
    create_table :equipment do |t|
      t.string :name, null: false
      t.boolean :weapon, null: false

      t.timestamps
    end
    add_index :equipment, :name, unique: true

    create_table :mech_equipment do |t|
      t.references :equipment, null: false, foreign_key: true
      t.references :mech_variant, null: false, foreign_key: true
      t.integer :count, null: false
      t.boolean :rear_facing, null: false, default: false

      t.timestamps
    end
    add_index :mech_equipment,
      %i[mech_variant_id equipment_id rear_facing],
      unique: true,
      name: "unique_mech_variant_equipment"

    create_table :mech_chassis do |t|
      t.string :name, null: false
      t.boolean :biped, null: false

      t.timestamps
    end
    add_index :mech_chassis, :name, unique: true

    change_table :mech_variants do |t|
      t.string :heat_sinks
      t.integer :walking_mp
      t.integer :jumping_mp
      t.integer :front_armor
      t.integer :rear_armor
      t.string :engine_model
      t.string :structure_type
      t.string :armor_type
      t.references :mech_chassis, null: true, foreign_key: true
    end
  end
end
