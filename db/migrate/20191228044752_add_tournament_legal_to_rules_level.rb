# frozen_string_literal: true

class AddTournamentLegalToRulesLevel < ActiveRecord::Migration[6.0]
  def up
    change_table :rules_levels, bulk: true do |t|
      t.boolean :tournament_legal
    end

    tournament_legal_rules_levels = %w[Introductory Standard]
    RulesLevel.find_each do |rules_level|
      rules_level.tournament_legal =
        tournament_legal_rules_levels.include?(rules_level.name)
      rules_level.save!
    end

    change_table :rules_levels, bulk: true do |t|
      t.change :tournament_legal, :boolean, null: false
    end
  end

  def down
    change_table :rules_levels, bulk: true do |t|
      t.remove :tournament_legal
    end
  end

  class RulesLevel < ApplicationRecord
    validates :name, presence: true
    validates :name, uniqueness: true

    has_many :mech_variants, inverse_of: :rules_level
  end
end
