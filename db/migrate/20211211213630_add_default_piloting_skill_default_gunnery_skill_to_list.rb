class AddDefaultPilotingSkillDefaultGunnerySkillToList < ActiveRecord::Migration[7.0]
  def change
    add_column :lists, :default_piloting_skill, :integer, null: false, default: 5
    add_column :lists, :default_gunnery_skill, :integer, null: false, default: 4
  end
end
