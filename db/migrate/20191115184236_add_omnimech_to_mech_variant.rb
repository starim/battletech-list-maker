# frozen_string_literal: true

class AddOmnimechToMechVariant < ActiveRecord::Migration[6.0]
  def change
    add_column :mech_variants, :omnimech, :boolean, null: false, default: false
  end
end
