class RemoveOwnerNameFromLists < ActiveRecord::Migration[6.0]
  def change
    remove_column :lists, :owner_name, :text
  end
end
