class CreateLists < ActiveRecord::Migration[6.0]
  def change
    create_table :lists do |t|
      t.string :name,                 null: false
      t.string :owner_name,           null: false
      t.integer :target_battle_value, null: false
      t.references :era,              foreign_key: true
      t.references :faction_category, foreign_key: true
      t.references :rules_level,      foreign_key: true

      t.timestamps
    end

    add_index :lists, :name, unique: true

    create_table :list_entries do |t|
      t.references :list,             null: false, foreign_key: true
      t.references :battle_value,     null: false, foreign_key: true

      t.timestamps
    end
  end
end
