class ChangeListIdToUuid < ActiveRecord::Migration[6.0]
  def up
     # Add UUID columns
    add_column :lists,    :uuid, :uuid, null: false, default: -> { "gen_random_uuid()" }

    # Add UUID columns for associations
    add_column :list_entries, :list_uuid, :uuid, null: true

    # Populate UUID columns for associations
    execute <<-SQL
      UPDATE list_entries SET list_uuid = lists.uuid
      FROM lists WHERE list_entries.list_id = lists.id;
    SQL

    # Make the reference column null
    change_column_null :list_entries, :list_uuid, false

    # Migrate UUID to ID for association
    remove_column :list_entries, :list_id
    rename_column :list_entries, :list_uuid, :list_id

    # Re-add index for association
    add_index :list_entries, :list_id

    # Migrate primary keys from UUIDs to IDs
    remove_column :lists,         :id
    rename_column :lists,         :uuid, :id
    execute "ALTER TABLE lists    ADD PRIMARY KEY (id);"

    # Re-add foreign key
    add_foreign_key :list_entries, :lists
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
