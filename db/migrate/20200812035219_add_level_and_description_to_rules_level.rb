class AddLevelAndDescriptionToRulesLevel < ActiveRecord::Migration[6.0]
  class RulesLevel < ApplicationRecord
    validates :name, :level, :description, presence: true
    validates :name, :level, uniqueness: true
    validates :level, numericality: { only_integer: true, greater_than: 0 }

    has_many :mech_variants, inverse_of: :rules_level, dependent:
      :restrict_with_error
    has_many :lists, inverse_of: :rules_level, dependent: :restrict_with_error
  end

  def change
    change_table :rules_levels, bulk: true do |t|
      t.integer :level, null: true
      t.text :description, null: true
    end

    add_index :rules_levels, :level, unique: true

    {
      "Introductory" => {
        level: 1,
        description: <<~DESCRIPTION.squish,
        Introductory rules cover the most basic mechs and technologies that are
        found in the beginner box set. Infantry and vehicles are not allowed at
        this rules level.
        DESCRIPTION
      },
      "Standard" => {
        level: 2,
        description: <<~DESCRIPTION.squish,
        Standard rules cover all the rules found in the Total Warfare core rule
        book.  As the name implies, this is the typical rules level used for
        Battletech games and tournaments.
        DESCRIPTION
      },
      "Advanced" => {
        level: 3,
        description: <<~DESCRIPTION.squish,
        Advanced rules cover flavorful but possibly unbalanced rules, or
        mechanics that are sufficiently complex that they would slow down
        tournament play. Artillery, dual cockpit mechs, and mech design quirk
        rules are some of those rules included under the advanced rules and are
        not tournament legal.
        DESCRIPTION
      },
    }.each do |rules_level_name, attributes|
      rules_level = RulesLevel.find_by(name: rules_level_name)
      if rules_level
        rules_level.update_attributes!(attributes)
      end
    end

    change_table :rules_levels, bulk: true do |t|
      t.change :level, :integer, null: false
      t.change :description, :text, null: false
    end
  end
end
