class AddSarnaUrlAndImageUrlToMechChassis < ActiveRecord::Migration[6.0]
  def change
    add_column :mech_chassis, :sarna_url, :string, null: false
  end
end
