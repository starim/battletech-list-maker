# frozen_string_literal: true

class ScopeChassisUniquenessToBothIsAndClanName < ActiveRecord::Migration[6.0]
  def up
    remove_index :mech_chassis, :name
    add_index :mech_chassis, %i[name clan_name], unique: true
  end

  def down
    remove_index :mech_chassis, %i[name clan_name]
    add_index :mech_chassis, :name
  end
end
