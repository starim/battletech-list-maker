# frozen_string_literal: true

class ImportMechsAtOnce < ActiveRecord::Migration[6.0]
  def up
    remove_column :mech_chassis, :biped

    change_table :mech_variants, bulk: true do |t|
      t.change :heat_sinks, :string, null: false
      t.change :walking_mp, :integer, null: false
      t.change :jumping_mp, :integer, null: false
      t.change :front_armor, :integer, null: false
      t.change :rear_armor, :integer, null: false
      t.change :engine_model, :string, null: false
      t.change :structure_type, :string, null: false
      t.change :armor_type, :string, null: false
      t.change :mech_chassis_id, :integer, null: false
    end
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
