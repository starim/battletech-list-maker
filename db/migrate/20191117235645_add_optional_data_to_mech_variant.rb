# frozen_string_literal: true

class AddOptionalDataToMechVariant < ActiveRecord::Migration[6.0]
  def up
    change_table :mech_variants, bulk: true do |t|
      t.remove :name, :technology_base_id
      t.string :designation, null: false
      t.string :nickname
    end
    add_index :mech_variants, %i[designation nickname], unique: true

    change_table :mech_chassis, bulk: true do |t|
      t.string :clan_name
      t.integer :technology_base_id, null: false
    end
    add_index :mech_chassis, :clan_name, unique: true
    add_foreign_key :mech_chassis, :technology_bases, column: :technology_base_id
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
