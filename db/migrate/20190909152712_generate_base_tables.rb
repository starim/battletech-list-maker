# frozen_string_literal: true

class GenerateBaseTables < ActiveRecord::Migration[6.0]
  def up
    execute('
      CREATE TABLE source_books (
          id serial PRIMARY KEY,
          name text NOT NULL UNIQUE
      );

      CREATE TABLE rules_levels (
          id serial PRIMARY KEY,
          name text NOT NULL UNIQUE
      );

      CREATE TABLE eras (
          id serial PRIMARY KEY,
          name text NOT NULL UNIQUE,
          start_year integer NOT NULL,
          end_year integer NOT NULL
      );

      CREATE TABLE faction_categories (
          id serial PRIMARY KEY,
          name text NOT NULL UNIQUE
      );

      CREATE TABLE factions (
          id serial PRIMARY KEY,
          name text NOT NULL UNIQUE,
          faction_category_id integer NOT NULL REFERENCES faction_categories(id)
      );

      CREATE TABLE technology_bases (
          id serial PRIMARY KEY,
          name text NOT NULL UNIQUE
      );

      CREATE TABLE mech_variants (
          id serial PRIMARY KEY,
          name text NOT NULL UNIQUE,
          tonnage integer NOT NULL CONSTRAINT positive_tonnage CHECK (tonnage > 0),
          technology_base_id integer NOT NULL REFERENCES technology_bases(id),
          base_battle_value integer NOT NULL CONSTRAINT positive_bv CHECK (base_battle_value > 0),
          cbill_cost integer,
          role text NOT NULL,
          rules_level_id integer NOT NULL REFERENCES rules_levels(id),
          year_introduced integer NOT NULL,
          mul_url text NOT NULL UNIQUE
      );

      CREATE TABLE mech_source_books (
          id serial PRIMARY KEY,
          mech_variant_id INTEGER NOT NULL REFERENCES mech_variants(id),
          source_book_id INTEGER NOT NULL REFERENCES source_books(id),
          CONSTRAINT unique_mech_source_books unique(mech_variant_id, source_book_id)
      );

      CREATE TABLE availability (
          id serial PRIMARY KEY,
          era_id INTEGER NOT NULL REFERENCES eras(id),
          mech_variant_id INTEGER NOT NULL REFERENCES mech_variants(id),
          faction_id INTEGER NOT NULL REFERENCES factions(id),
          CONSTRAINT unique_availability_era_variant unique(era_id, mech_variant_id, faction_id)
      );

      CREATE TABLE battle_values (
          id serial PRIMARY KEY,
          mech_variant_id integer NOT NULL REFERENCES mech_variants(id),
          piloting_skill integer NOT NULL CONSTRAINT valid_piloting_skill_range CHECK (piloting_skill >= 0 AND piloting_skill <= 8),
          gunnery_skill integer NOT NULL CONSTRAINT valid_gunnery_skill_range CHECK (gunnery_skill >= 0 AND gunnery_skill <= 8),
          battle_value integer NOT NULL CONSTRAINT positive_bv CHECK (battle_value >= 0),
          CONSTRAINT unique_battle_value_for_skills unique(mech_variant_id, piloting_skill, gunnery_skill)
      );
    ')
  end

  def down
    drop_table battle_values
    drop_table availability
    drop_table mech_source_books
    drop_table mech_variants
    drop_table technology_bases
    drop_table factions
    drop_table categories
    drop_table eras
    drop_table rules_levels
    drop_table source_books
  end
end
