# frozen_string_literal: true

class AddDescriptionToEra < ActiveRecord::Migration[6.0]
  class Era < ApplicationRecord
    validates :name, :start_year, :end_year, presence: true
    validates :name, uniqueness: true

    validates :start_year, :end_year, numericality: { only_integer: true }

    has_many :availability, inverse_of: :era
    has_many :mech_variants, through: :availability, inverse_of: :eras
  end

  def up
    add_column :eras, :description, :text

    era_descriptions = {
      "Star League" => "The golden age of the Inner Sphere. All of humanity is united under a single federation known as the Star League.",
      "Early Succession War" => "The great houses vie to claim dominion over the Inner Sphere after the fall of the Star League.",
      "Late Succession War - LosTech" => "The Third Succession War rages. The massive destruction from hundreds of years of total war has decimated the Inner Sphere's technological base.",
      "Late Succession War - Renaissance" => "The Inner Sphere rediscovers technologies lost in the destruction of the succession wars and for the first time in centuries, Star League technologies appear again.",
      "Clan Invasion" => "An overwhelming invasion by mysterious forces from the deep periphery--the Clans--force the Inner Sphere to unite in order to survive the onslaught.",
      "Civil War" => "The supremely powerful Federated Commonwealth shatters into civil war and once again the entire Inner Sphere is dragged into open conflict.",
    }

    Era.find_each do |era|
      era.description = era_descriptions[era.name]
      era.save!
    end

    change_column :eras, :description, :text, null: true
  end

  def down
    remove_column :eras, :description
  end
end
