# frozen_string_literal: true

class AddModelAvailability < ActiveRecord::Migration[6.0]
  def change
    change_table :mech_chassis, bulk: true do |t|
      t.date :plastic_model_release_date
      t.boolean :plastic_model_owned, null: false, default: false
    end
  end
end
