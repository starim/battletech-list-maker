# frozen_string_literal: true

class AddRolesTable < ActiveRecord::Migration[6.0]
  def up
    create_table :roles, bulk: true do |t|
      t.string :name, null: false

      t.timestamps
    end
    add_index :roles, :name, unique: true

    Role.create! name: "Scout"
    Role.create! name: "Missile Boat"
    Role.create! name: "Striker"
    Role.create! name: "Sniper"
    Role.create! name: "Brawler"
    Role.create! name: "Ambusher"
    Role.create! name: "Skirmisher"
    Role.create! name: "Juggernaut"

    change_table :mech_variants, bulk: true do |t|
      t.references :role, foreign_key: true
    end

    MechVariant.find_each do |mech|
      if mech.role != ""
        role = Role.find_by(name: mech.role)
        raise "No Role found matching name \"#{mech.role}\"" if role.nil?

        mech.role_id = role.id
        mech.save!
      end
    end

    change_table :mech_variants, bulk: true do |t|
      t.remove :role
    end
  end

  def down
    change_table :mech_variants, bulk: true do |t|
      t.string :role
    end

    MechVariant.find_each do |mech|
      mech.role = if mech.role_id.nil?
        ""
      else
        Role.find_by(id: mech.role_id).name
                  end
      mech.save!
    end

    change_table :mech_variants, bulk: true do |t|
      t.change :role, :string, null: false
      t.remove :role_id
    end

    drop_table :roles
  end

  class Role < ApplicationRecord
    validates :name, presence: true
    validates :name, uniqueness: true
  end

  class MechVariant < ApplicationRecord
    validates :designation, :tonnage, :base_battle_value, :year_introduced,
      :mul_url, :heat_sinks, :walking_mp, :jumping_mp, :front_armor, :rear_armor,
      :engine_model, :structure_type, :armor_type, presence: true
    validates :mul_url, uniqueness: true
    validates :designation, uniqueness: { scope: [:mech_chassis_id] }
    validates :tonnage, :base_battle_value, numericality:
      { only_integer: true, greater_than: 0 }
    validates :cbill_cost, numericality:
      { only_integer: true, greater_than: 0, allow_nil: true }
    validates :role, length:
      { minimum: 0, allow_nil: false, message: "can't be nil" }
    validates :year_introduced, numericality: { only_integer: true }
    validates :walking_mp, :jumping_mp, :front_armor, :rear_armor, numericality:
      { only_integer: true, greater_than_or_equal_to: 0 }

    belongs_to :chassis, class_name: "MechChassis", foreign_key:
      :mech_chassis_id, inverse_of: :mech_variants
    belongs_to :rules_level, optional: false, inverse_of: :mech_variants
    belongs_to :technology_base, optional: false, inverse_of: :mech_variants

    has_many :mech_source_books, inverse_of: :mech_variant, dependent: :destroy
    has_many :source_books, through: :mech_source_books, inverse_of: :mech_variants
    has_many :availability, inverse_of: :mech_variant, dependent: :destroy
    has_many :eras, through: :availability, inverse_of: :mech_variants
    has_many :battle_values, inverse_of: :mech_variant, dependent: :destroy
    has_many :mech_equipment, inverse_of: :mech_variant, dependent: :destroy
    has_many :equipment, through: :mech_equipment, inverse_of: :mech_variants

    def full_name
      "#{chassis.name} #{designation}"
    end

    def total_armor
      front_armor + rear_armor
    end
  end
end
