# frozen_string_literal: true

class ScopeMechVariantUniquenessToChassis < ActiveRecord::Migration[6.0]
  def up
    remove_index :mech_variants, %i[designation nickname]
    add_index(
      :mech_variants,
      %i[designation mech_chassis_id nickname],
      unique: true,
      name: "mech_variant_unique_designation_index"
    )
  end

  def down
    remove_index :mech_variants, %i[designation mech_chassis_id nickname]
    add_index :mech_variants, %i[designation nickname], unique: true
  end
end
