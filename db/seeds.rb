era_star_league = Era.create!(
  name: "Star League",
  start_year: 2571,
  end_year: 2780,
  description: <<~DESCRIPTION.squish
    The golden age of the Inner Sphere. All of humanity is united under a
    single federation known as the Star League.
  DESCRIPTION
)
era_early_succession_war = Era.create!(
  name: "Early Succession War",
  start_year: 2781,
  end_year: 2900,
  description: <<~DESCRIPTION.squish
    The great houses vie to claim dominion over the Inner Sphere after the fall
    of the Star League.
  DESCRIPTION
)
era_late_succession_war = Era.create!(
  name: "Late Succession War",
  start_year: 2901,
  end_year: 3049,
  description: <<~DESCRIPTION.squish
    The Succession Wars rage on. The massive destruction from hundreds of years
    of total war has decimated the Inner Sphere's technological base. With the
    discovery of the Helm Core near the end of this era, the Inner Sphere
    rediscovers some of the technologies lost in the destruction and for the
    first time in centuries, Star League technologies appear again.
  DESCRIPTION
)
era_clan_invasion = Era.create!(
  name: "Clan Invasion",
  start_year: 3050,
  end_year: 3061,
  description: <<~DESCRIPTION.squish
    An overwhelming invasion by mysterious forces from the deep periphery--the
    Clans--force the Inner Sphere to unite in order to survive the onslaught.
  DESCRIPTION
)
era_civil_war = Era.create!(
  name: "FedCom Civil War",
  start_year: 3062,
  end_year: 3067,
  description: <<~DESCRIPTION.squish
    The supremely powerful Federated Commonwealth shatters into civil war and
    once again the entire Inner Sphere is dragged into open conflict.
  DESCRIPTION
)

faction_category_inner_sphere = FactionCategory.create!(name: "Inner Sphere")
faction_category_comstar = FactionCategory.create!(name: "Comstar")
faction_category_clan = FactionCategory.create!(name: "Clan")
faction_category_periphery = FactionCategory.create!(name: "Periphery")
faction_category_mercenary = FactionCategory.create!(name: "Mercenary")

Faction.create!(name: "Capellan Confederation", faction_category: faction_category_inner_sphere)
Faction.create!(name: "Comstar", faction_category: faction_category_comstar)
Faction.create!(name: "Draconis Combine", faction_category: faction_category_inner_sphere)
Faction.create!(name: "Federated Commonwealth", faction_category: faction_category_inner_sphere)
Faction.create!(name: "Federated Suns", faction_category: faction_category_inner_sphere)
Faction.create!(name: "Free Rasalhague Republic", faction_category: faction_category_inner_sphere)
Faction.create!(name: "Free Worlds League", faction_category: faction_category_inner_sphere)
Faction.create!(name: "Lyran Alliance", faction_category: faction_category_inner_sphere)
Faction.create!(name: "Lyran Commonwealth", faction_category: faction_category_inner_sphere)
Faction.create!(name: "St. Ives Compact", faction_category: faction_category_inner_sphere)
Faction.create!(name: "Terran Hegemony", faction_category: faction_category_inner_sphere)
Faction.create!(name: "Word of Blake", faction_category: faction_category_inner_sphere)
Faction.create!(name: "Clan Diamond Shark", faction_category: faction_category_clan)
Faction.create!(name: "Clan Ghost Bear", faction_category: faction_category_clan)
Faction.create!(name: "Clan Hell's Horses", faction_category: faction_category_clan)
Faction.create!(name: "Clan Jade Falcon", faction_category: faction_category_clan)
Faction.create!(name: "Clan Nova Cat", faction_category: faction_category_clan)
Faction.create!(name: "Clan Smoke Jaguar", faction_category: faction_category_clan)
Faction.create!(name: "Clan Snow Raven", faction_category: faction_category_clan)
Faction.create!(name: "Clan Wolf", faction_category: faction_category_clan)
Faction.create!(name: "Clan Wolf-in-Exile", faction_category: faction_category_clan)
Faction.create!(name: "Clan Blood Spirit", faction_category: faction_category_clan)
Faction.create!(name: "Clan Burrock", faction_category: faction_category_clan)
Faction.create!(name: "Clan Cloud Cobra", faction_category: faction_category_clan)
Faction.create!(name: "Clan Coyote", faction_category: faction_category_clan)
Faction.create!(name: "Clan Fire Mandrill", faction_category: faction_category_clan)
Faction.create!(name: "Clan Goliath Scorpion", faction_category: faction_category_clan)
Faction.create!(name: "Clan Ice Hellion", faction_category: faction_category_clan)
Faction.create!(name: "Clan Star Adder", faction_category: faction_category_clan)
Faction.create!(name: "Clan Steel Viper", faction_category: faction_category_clan)
Faction.create!(name: "Circinus Federation", faction_category: faction_category_periphery)
Faction.create!(name: "Magistracy of Canopus", faction_category: faction_category_periphery)
Faction.create!(name: "Marian Hegemony", faction_category: faction_category_periphery)
Faction.create!(name: "Outworlds Alliance", faction_category: faction_category_periphery)
Faction.create!(name: "Pirates", faction_category: faction_category_periphery)
Faction.create!(name: "Rim Worlds Republic - Home Guard", faction_category: faction_category_periphery)
Faction.create!(name: "Rim Worlds Republic - Terran Corps", faction_category: faction_category_periphery)
Faction.create!(name: "Taurian Concordat", faction_category: faction_category_periphery)
Faction.create!(name: "Kell Hounds", faction_category: faction_category_mercenary)
Faction.create!(name: "Mercenary", faction_category: faction_category_mercenary)
Faction.create!(name: "Wolf's Dragoons", faction_category: faction_category_mercenary)
Faction.create!(name: "Solaris 7", faction_category: faction_category_inner_sphere)
Faction.create!(name: "Star League (Second)", faction_category: faction_category_inner_sphere)
Faction.create!(name: "Star League Regular", faction_category: faction_category_inner_sphere)
Faction.create!(name: "Star League Royal", faction_category: faction_category_inner_sphere)

RulesLevel.create!(name: "Introductory", tournament_legal: true, level: 1, description: <<-DESCRIPTION.squish)
Introductory rules cover the most basic mechs and technologies that are found
in the beginner box set. Infantry and vehicles are not allowed at this rules
level.
DESCRIPTION

RulesLevel.create!(name: "Standard", tournament_legal: true, level: 2, description: <<-DESCRIPTION.squish)
Standard rules cover all the rules found in the Total Warfare core rule book.
As the name implies, this is the typical rules level used for Battletech games
and tournaments.
DESCRIPTION

RulesLevel.create!(name: "Advanced", tournament_legal: false, level: 3, description: <<-DESCRIPTION.squish)
Advanced rules cover flavorful but possibly unbalanced rules, or mechanics that
are sufficiently complex that they would slow down tournament play. Artillery,
dual cockpit mechs, and mech design quirk rules are some of those rules
included under the advanced rules and are not tournament legal.
DESCRIPTION

TechnologyBase.create!(name: "Inner Sphere")
TechnologyBase.create!(name: "Clan")
TechnologyBase.create!(name: "Mixed")
TechnologyBase.create!(name: "Primitive")

Equipment.create!(name: "AC/2", weapon: true)
Equipment.create!(name: "AC/5", weapon: true)
Equipment.create!(name: "AC/10", weapon: true)
Equipment.create!(name: "AC/20", weapon: true)
Equipment.create!(name: "Ultra AC/2", weapon: true)
Equipment.create!(name: "Ultra AC/5", weapon: true)
Equipment.create!(name: "Ultra AC/10", weapon: true)
Equipment.create!(name: "Ultra AC/20", weapon: true)
Equipment.create!(name: "Clan Ultra AC/2", weapon: true)
Equipment.create!(name: "Clan Ultra AC/5", weapon: true)
Equipment.create!(name: "Clan Ultra AC/10", weapon: true)
Equipment.create!(name: "Clan Ultra AC/20", weapon: true)
Equipment.create!(name: "LBX AC/2", weapon: true)
Equipment.create!(name: "LBX AC/5", weapon: true)
Equipment.create!(name: "LBX AC/10", weapon: true)
Equipment.create!(name: "LBX AC/20", weapon: true)
Equipment.create!(name: "Clan LBX AC/2", weapon: true)
Equipment.create!(name: "Clan LBX AC/5", weapon: true)
Equipment.create!(name: "Clan LBX AC/10", weapon: true)
Equipment.create!(name: "Clan LBX AC/20", weapon: true)
Equipment.create!(name: "RAC/2", weapon: true)
Equipment.create!(name: "RAC/5", weapon: true)
Equipment.create!(name: "Light AC/2", weapon: true)
Equipment.create!(name: "Light AC/5", weapon: true)
Equipment.create!(name: "SRM 2", weapon: true)
Equipment.create!(name: "SRM 4", weapon: true)
Equipment.create!(name: "SRM 6", weapon: true)
Equipment.create!(name: "Clan SRM 2", weapon: true)
Equipment.create!(name: "Clan SRM 4", weapon: true)
Equipment.create!(name: "Clan SRM 6", weapon: true)
Equipment.create!(name: "Streak SRM 2", weapon: true)
Equipment.create!(name: "Streak SRM 4", weapon: true)
Equipment.create!(name: "Streak SRM 6", weapon: true)
Equipment.create!(name: "Clan Streak SRM 2", weapon: true)
Equipment.create!(name: "Clan Streak SRM 4", weapon: true)
Equipment.create!(name: "Clan Streak SRM 6", weapon: true)
Equipment.create!(name: "One-Shot SRM 2", weapon: true)
Equipment.create!(name: "One-Shot SRM 4", weapon: true)
Equipment.create!(name: "One-Shot SRM 6", weapon: true)
Equipment.create!(name: "One-Shot Clan Streak SRM 4", weapon: true)
Equipment.create!(name: "One-Shot Streak SRM 2", weapon: true)
Equipment.create!(name: "One-Shot Streak SRM 4", weapon: true)
Equipment.create!(name: "One-Shot Streak SRM 6", weapon: true)
Equipment.create!(name: "MRM 10", weapon: true)
Equipment.create!(name: "MRM 20", weapon: true)
Equipment.create!(name: "MRM 30", weapon: true)
Equipment.create!(name: "MRM 40", weapon: true)
Equipment.create!(name: "LRM 5", weapon: true)
Equipment.create!(name: "LRM 10", weapon: true)
Equipment.create!(name: "LRM 15", weapon: true)
Equipment.create!(name: "LRM 20", weapon: true)
Equipment.create!(name: "Clan LRM 5", weapon: true)
Equipment.create!(name: "Clan LRM 10", weapon: true)
Equipment.create!(name: "Clan LRM 15", weapon: true)
Equipment.create!(name: "Clan LRM 20", weapon: true)
Equipment.create!(name: "Rocket Launcher 10", weapon: true)
Equipment.create!(name: "Rocket Launcher 15", weapon: true)
Equipment.create!(name: "Rocket Launcher 20", weapon: true)
Equipment.create!(name: "ATM 3", weapon: true)
Equipment.create!(name: "ATM 6", weapon: true)
Equipment.create!(name: "ATM 9", weapon: true)
Equipment.create!(name: "ATM 12", weapon: true)
Equipment.create!(name: "Clan ATM 3", weapon: true)
Equipment.create!(name: "Clan ATM 6", weapon: true)
Equipment.create!(name: "Clan ATM 9", weapon: true)
Equipment.create!(name: "Clan ATM 12", weapon: true)
Equipment.create!(name: "Arrow IV", weapon: true)
Equipment.create!(name: "Clan Arrow IV", weapon: true)
Equipment.create!(name: "Arrow V", weapon: true)
Equipment.create!(name: "Thunderbolt 5", weapon: true)
Equipment.create!(name: "Thunderbolt 10", weapon: true)
Equipment.create!(name: "Thunderbolt 15", weapon: true)
Equipment.create!(name: "Thunderbolt 20", weapon: true)
Equipment.create!(name: "Small Laser", weapon: true)
Equipment.create!(name: "Small Pulse Laser", weapon: true)
Equipment.create!(name: "Clan Small Pulse Laser", weapon: true)
Equipment.create!(name: "ER Small Laser", weapon: true)
Equipment.create!(name: "Clan ER Small Laser", weapon: true)
Equipment.create!(name: "Heavy Small Laser", weapon: true)
Equipment.create!(name: "ER Micro Laser", weapon: true)
Equipment.create!(name: "Micro Pulse Laser", weapon: true)
Equipment.create!(name: "Clan Micro Pulse Laser", weapon: true)
Equipment.create!(name: "Clan ER Micro Laser", weapon: true)
Equipment.create!(name: "Medium Laser", weapon: true)
Equipment.create!(name: "Medium Pulse Laser", weapon: true)
Equipment.create!(name: "Clan Medium Pulse Laser", weapon: true)
Equipment.create!(name: "ER Medium Laser", weapon: true)
Equipment.create!(name: "Clan ER Medium Laser", weapon: true)
Equipment.create!(name: "Heavy Medium Laser", weapon: true)
Equipment.create!(name: "Large Laser", weapon: true)
Equipment.create!(name: "Large Pulse Laser", weapon: true)
Equipment.create!(name: "Clan Large Pulse Laser", weapon: true)
Equipment.create!(name: "ER Large Laser", weapon: true)
Equipment.create!(name: "Clan ER Large Laser", weapon: true)
Equipment.create!(name: "Small X-Pulse Laser", weapon: true)
Equipment.create!(name: "Medium X-Pulse Laser", weapon: true)
Equipment.create!(name: "Large X-Pulse Laser", weapon: true)
Equipment.create!(name: "Heavy Large Laser", weapon: true)
Equipment.create!(name: "Clan Heavy Small Laser", weapon: true)
Equipment.create!(name: "Clan Heavy Medium Laser", weapon: true)
Equipment.create!(name: "Clan Heavy Large Laser", weapon: true)
Equipment.create!(name: "Blazer Cannon", weapon: true)
Equipment.create!(name: "PPC", weapon: true)
Equipment.create!(name: "ER PPC", weapon: true)
Equipment.create!(name: "Clan ER PPC", weapon: true)
Equipment.create!(name: "Snub-Nose PPC", weapon: true)
Equipment.create!(name: "Light PPC", weapon: true)
Equipment.create!(name: "Heavy PPC", weapon: true)
Equipment.create!(name: "Flamer", weapon: true)
Equipment.create!(name: "Clan Flamer", weapon: true)
Equipment.create!(name: "Heavy Flamer", weapon: true)
Equipment.create!(name: "Plasma Rifle", weapon: true)
Equipment.create!(name: "Machine Gun", weapon: true)
Equipment.create!(name: "Clan Light Machine Gun", weapon: true)
Equipment.create!(name: "Light Machine Gun", weapon: true)
Equipment.create!(name: "Heavy Machine Gun", weapon: true)
Equipment.create!(name: "Machine Gun Array", weapon: true)
Equipment.create!(name: "Gauss Rifle", weapon: true)
Equipment.create!(name: "Clan Gauss Rifle", weapon: true)
Equipment.create!(name: "Light Gauss Rifle", weapon: true)
Equipment.create!(name: "Clan Light Gauss Rifle", weapon: true)
Equipment.create!(name: "Heavy Gauss Rifle", weapon: true)
Equipment.create!(name: "Hyper-Assault Gauss Rifle 20", weapon: true)
Equipment.create!(name: "Hyper-Assault Gauss Rifle 30", weapon: true)
Equipment.create!(name: "Hyper-Assault Gauss Rifle 40", weapon: true)
Equipment.create!(name: "Magshot", weapon: true)
Equipment.create!(name: "Mortar 1", weapon: true)
Equipment.create!(name: "Mortar 2", weapon: true)
Equipment.create!(name: "Mortar 4", weapon: true)
Equipment.create!(name: "Mortar 8", weapon: true)
Equipment.create!(name: "Long Tom Artillery", weapon: true)
Equipment.create!(name: "Sniper Artillery", weapon: true)
Equipment.create!(name: "Fluid Gun", weapon: true)
Equipment.create!(name: "Sword", weapon: true)
Equipment.create!(name: "Hatchet", weapon: true)
Equipment.create!(name: "A-Pod", weapon: true)
Equipment.create!(name: "M-Pod", weapon: true)
Equipment.create!(name: "Vehicular Grenade Launcher", weapon: true)

Equipment.create!(name: "Triple-Strength Myomer", weapon: false)
Equipment.create!(name: "MASC", weapon: false)
Equipment.create!(name: "Clan MASC", weapon: false)
Equipment.create!(name: "Guardian ECM", weapon: false)
Equipment.create!(name: "Clan ECM", weapon: false)
Equipment.create!(name: "Beagle Active Probe", weapon: false)
Equipment.create!(name: "Clan Active Probe", weapon: false)
Equipment.create!(name: "Clan Light Active Probe", weapon: false)
Equipment.create!(name: "TAG", weapon: false)
Equipment.create!(name: "Clan TAG", weapon: false)
Equipment.create!(name: "Clan Light TAG", weapon: false)
Equipment.create!(name: "Taser", weapon: false)
Equipment.create!(name: "Narc Beacon", weapon: false)
Equipment.create!(name: "Clan Narc Beacon", weapon: false)
Equipment.create!(name: "Improved Narc Beacon", weapon: false)
Equipment.create!(name: "C3 Command Unit", weapon: false)
Equipment.create!(name: "C3 Command Unit with TAG", weapon: false)
Equipment.create!(name: "C3 Slave Unit", weapon: false)
Equipment.create!(name: "C3i Unit", weapon: false)
Equipment.create!(name: "AMS", weapon: false)
Equipment.create!(name: "Clan AMS", weapon: false)
Equipment.create!(name: "LAMS", weapon: false)

Role.find_or_create_by! name: "Scout"
Role.find_or_create_by! name: "Missile Boat"
Role.find_or_create_by! name: "Striker"
Role.find_or_create_by! name: "Sniper"
Role.find_or_create_by! name: "Brawler"
Role.find_or_create_by! name: "Ambusher"
Role.find_or_create_by! name: "Skirmisher"
Role.find_or_create_by! name: "Juggernaut"
