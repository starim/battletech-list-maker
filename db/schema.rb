# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_12_20_184538) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum"
    t.datetime "created_at", precision: 6, null: false
    t.string "service_name", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "availability", id: :serial, force: :cascade do |t|
    t.integer "era_id", null: false
    t.integer "mech_variant_id", null: false
    t.integer "faction_id", null: false
    t.index ["era_id", "mech_variant_id", "faction_id"], name: "unique_availability_era_variant", unique: true
  end

  create_table "battle_values", id: :serial, force: :cascade do |t|
    t.integer "mech_variant_id", null: false
    t.integer "piloting_skill", null: false
    t.integer "gunnery_skill", null: false
    t.integer "battle_value", null: false
    t.index ["mech_variant_id", "piloting_skill", "gunnery_skill"], name: "unique_battle_value_for_skills", unique: true
    t.check_constraint "(gunnery_skill >= 0) AND (gunnery_skill <= 8)", name: "valid_gunnery_skill_range"
    t.check_constraint "(piloting_skill >= 0) AND (piloting_skill <= 8)", name: "valid_piloting_skill_range"
    t.check_constraint "battle_value >= 0", name: "positive_bv"
  end

  create_table "equipment", force: :cascade do |t|
    t.string "name", null: false
    t.boolean "weapon", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_equipment_on_name", unique: true
  end

  create_table "eras", id: :serial, force: :cascade do |t|
    t.text "name", null: false
    t.integer "start_year", null: false
    t.integer "end_year", null: false
    t.text "description"
    t.index ["name"], name: "eras_name_key", unique: true
  end

  create_table "faction_categories", id: :serial, force: :cascade do |t|
    t.text "name", null: false
    t.index ["name"], name: "faction_categories_name_key", unique: true
  end

  create_table "factions", id: :serial, force: :cascade do |t|
    t.text "name", null: false
    t.integer "faction_category_id", null: false
    t.index ["name"], name: "factions_name_key", unique: true
  end

  create_table "list_entries", force: :cascade do |t|
    t.bigint "battle_value_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.uuid "list_id", null: false
    t.index ["battle_value_id"], name: "index_list_entries_on_battle_value_id"
    t.index ["list_id"], name: "index_list_entries_on_list_id"
  end

  create_table "lists", id: :uuid, default: -> { "public.gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.integer "target_battle_value", null: false
    t.bigint "era_id"
    t.bigint "faction_category_id"
    t.bigint "rules_level_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "default_piloting_skill", default: 5, null: false
    t.integer "default_gunnery_skill", default: 4, null: false
    t.index ["era_id"], name: "index_lists_on_era_id"
    t.index ["faction_category_id"], name: "index_lists_on_faction_category_id"
    t.index ["name"], name: "index_lists_on_name", unique: true
    t.index ["rules_level_id"], name: "index_lists_on_rules_level_id"
  end

  create_table "mech_chassis", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "clan_name"
    t.date "plastic_model_release_date"
    t.boolean "plastic_model_owned", default: false, null: false
    t.string "sarna_url", null: false
    t.index ["clan_name"], name: "index_mech_chassis_on_clan_name", unique: true
    t.index ["name", "clan_name"], name: "index_mech_chassis_on_name_and_clan_name", unique: true
  end

  create_table "mech_equipment", force: :cascade do |t|
    t.bigint "equipment_id", null: false
    t.bigint "mech_variant_id", null: false
    t.integer "count", null: false
    t.boolean "rear_facing", default: false, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["equipment_id"], name: "index_mech_equipment_on_equipment_id"
    t.index ["mech_variant_id", "equipment_id", "rear_facing"], name: "unique_mech_variant_equipment", unique: true
    t.index ["mech_variant_id"], name: "index_mech_equipment_on_mech_variant_id"
  end

  create_table "mech_source_books", id: :serial, force: :cascade do |t|
    t.integer "mech_variant_id", null: false
    t.integer "source_book_id", null: false
    t.index ["mech_variant_id", "source_book_id"], name: "unique_mech_source_books", unique: true
  end

  create_table "mech_variants", id: :serial, force: :cascade do |t|
    t.integer "tonnage", null: false
    t.integer "base_battle_value", null: false
    t.integer "cbill_cost"
    t.integer "rules_level_id", null: false
    t.integer "year_introduced", null: false
    t.text "mul_url", null: false
    t.string "heat_sinks", null: false
    t.integer "walking_mp", null: false
    t.integer "jumping_mp", null: false
    t.integer "front_armor", null: false
    t.integer "rear_armor", null: false
    t.string "engine_model", null: false
    t.string "structure_type", null: false
    t.string "armor_type", null: false
    t.integer "mech_chassis_id", null: false
    t.boolean "omnimech", default: false, null: false
    t.string "designation", null: false
    t.bigint "technology_base_id", null: false
    t.bigint "role_id"
    t.text "full_name", null: false
    t.index ["full_name"], name: "index_mech_variants_on_full_name", unique: true
    t.index ["mech_chassis_id"], name: "index_mech_variants_on_mech_chassis_id"
    t.index ["mul_url"], name: "mech_variants_mul_url_key", unique: true
    t.index ["role_id"], name: "index_mech_variants_on_role_id"
    t.index ["technology_base_id"], name: "index_mech_variants_on_technology_base_id"
    t.check_constraint "base_battle_value > 0", name: "positive_bv"
    t.check_constraint "tonnage > 0", name: "positive_tonnage"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_roles_on_name", unique: true
  end

  create_table "rules_levels", id: :serial, force: :cascade do |t|
    t.text "name", null: false
    t.boolean "tournament_legal", null: false
    t.integer "level", null: false
    t.text "description", null: false
    t.index ["level"], name: "index_rules_levels_on_level", unique: true
    t.index ["name"], name: "rules_levels_name_key", unique: true
  end

  create_table "source_books", id: :serial, force: :cascade do |t|
    t.text "name", null: false
    t.index ["name"], name: "source_books_name_key", unique: true
  end

  create_table "technology_bases", id: :serial, force: :cascade do |t|
    t.text "name", null: false
    t.index ["name"], name: "technology_bases_name_key", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "availability", "eras", name: "availability_era_id_fkey"
  add_foreign_key "availability", "factions", name: "availability_faction_id_fkey"
  add_foreign_key "availability", "mech_variants", name: "availability_mech_variant_id_fkey"
  add_foreign_key "battle_values", "mech_variants", name: "battle_values_mech_variant_id_fkey"
  add_foreign_key "factions", "faction_categories", name: "factions_faction_category_id_fkey"
  add_foreign_key "list_entries", "battle_values"
  add_foreign_key "list_entries", "lists"
  add_foreign_key "lists", "eras"
  add_foreign_key "lists", "faction_categories"
  add_foreign_key "lists", "rules_levels"
  add_foreign_key "mech_equipment", "equipment"
  add_foreign_key "mech_equipment", "mech_variants"
  add_foreign_key "mech_source_books", "mech_variants", name: "mech_source_books_mech_variant_id_fkey"
  add_foreign_key "mech_source_books", "source_books", name: "mech_source_books_source_book_id_fkey"
  add_foreign_key "mech_variants", "mech_chassis"
  add_foreign_key "mech_variants", "roles"
  add_foreign_key "mech_variants", "rules_levels", name: "mech_variants_rules_level_id_fkey"
  add_foreign_key "mech_variants", "technology_bases", column: "technology_base_id"
end
