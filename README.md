# Battletech List Maker

A list maker for the Battletech tabletop game.


# Deployment

## First Time Setup

1. Create the user account to run the list maker: `useradd -G ssh-users -m battletech_list_maker`
2. Copy `config/master.key` to the home folder of the user who will run the list maker
3. Create a database user `battletech_list_maker` and grant it the privilege to create databases:
```
sudo -iu postgres
createuser --interactive
```
4. Run `mina setup`
5. Comment out the following deployment steps from config/deploy.rb:
  * running database migrations
  * precompiling assets
  * starting puma
6. Run `mina deploy`
7. Run `mina "rake[db:setup]" -v
8. Clone [the mech data repo](https://github.com/starim2/helm-core-fragment.git) into the list maker user's home folder
9. Run `mina "rake[import:masterunitlist[/home/battletech_list_maker/helm-core-fragment]]" -v
10. Copy the nginx configuration to `/etc/nginx/sites-available/` on the server, remove the .nginx extension, symlink it into `/etc/nginx/sites-enabled/`, and restart nginx
11. Copy `battletech-list-maker.service` to `/etc/systemd/system/` and then run `sudo systemctl daemon-reload`
12. Edit sudoers to allow the `battletech_list_maker` user to issue systemd commands for the `battletech-list-maker` service
```
battletech_list_maker ALL=(root) NOPASSWD: systemctl restart battletech-list-maker.service
```
13. Uncomment all the deployment tasks from step 4
14. Run `mina deploy`

## Regular Deployments

Make sure code is pushed up to origin master then run `mina deploy`

