source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby ">=3.0.2"

# Use local checkout of Rails
gem "rails", "~> 7.0.0"

# The traditional bundling and transpiling asset pipeline for Rails.
gem "sprockets-rails"

# Use postgresql as the database for Active Record
gem 'pg', '>= 0.18', '< 2.0'

# Use the Puma web server [https://github.com/puma/puma]
gem "puma", "~> 6.4.1"

# Use JavaScript with ESM import maps [https://github.com/rails/importmap-rails]
gem "importmap-rails"

# Hotwire's SPA-like page accelerator [https://turbo.hotwired.dev]
gem "turbo-rails", "~> 1.1"

# Hotwire's modest JavaScript framework [https://stimulus.hotwired.dev]
gem "stimulus-rails"

# Use Redis adapter to run Action Cable in production
gem "redis", "~> 4.0"

# Font Awesome https://github.com/bokmann/font-awesome-rails
gem 'font-awesome-rails', '~> 4.7', '>= 4.7.0.8'

# Use Kredis to get higher-level data types in Redis [https://github.com/rails/kredis]
# gem "kredis"

# Use Active Model has_secure_password [https://guides.rubyonrails.org/active_model_basics.html#securepassword]
# gem "bcrypt", "~> 3.1.7"

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem "tzinfo-data", platforms: %i[ mingw mswin x64_mingw jruby ]

# Use Active Storage variants [https://guides.rubyonrails.org/active_storage_overview.html#transforming-images]
# gem "image_processing", "~> 1.2"

# this gem is required in all environments because it's used in the seeds file
gem "factory_bot_rails", "~> 6.2.0"

# used by the importer rake task
gem "nokogiri", "~> 1.12.5"

# irb is a gem now instead of being packaged with Ruby. This is required to be
# able to open a Rails console.
gem 'irb'

gem 'json', '~> 2.7.1'

group :production do
  gem "sd_notify", "~> 0.1.1"
end

group :development, :test do
  # Start debugger with binding.b [https://github.com/ruby/debug]
  gem "debug", ">= 1.0.0", platforms: %i[ mri mingw x64_mingw ]

  gem "rspec-rails", "~> 5.0.2"
end

group :development do
  # Use console on exceptions pages [https://github.com/rails/web-console]
  gem "web-console", ">= 4.1.0"

  gem "awesome_print", "~> 1.9"
  # annotate doesn't have a version yet that's compatible with Rails 7
  # gem "annotate", "~> 3.1.1"

  # Mina is our deployment tool
  gem 'mina', '~> 1.2.4'
end

group :test do
  # Use system testing [https://guides.rubyonrails.org/testing.html#system-testing]
  gem "capybara", ">= 3.26"
  gem "selenium-webdriver", ">= 4.0.0"
  gem "webdrivers"
end
