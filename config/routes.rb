Rails.application.routes.draw do
  root to: "lists#index"

  resources :mech_variants, only: %i[index show]
  resources :lists, except: :destroy do
    resources :list_entries, shallow: true
  end
end
