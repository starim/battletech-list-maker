require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module BattletechListMaker
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.0

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")

    # custom configuration:

    config.MIN_SKILL_VALUE = 0
    config.MAX_SKILL_VALUE = 8

    # the name that should be displayed in messages relating to ownership of
    # models for mechs
    config.site_owner_name = 'Brent'

    config.pagination_page_size = 8
  end
end
