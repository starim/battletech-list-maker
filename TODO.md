* make the lists#index page live refresh from Hotwire
* "last updated at" column on list#index should count list entry changes as well, otherwise it's kind of meaningless
* update mech data parsing:
	* be able to read mtf files to get more mechs
	* be able to more reliably detect the presence of special equipment (e.g. Artemis IV FCS) and ammo counts
	* display new data:
		* ammo count for weapons (e.g. "AC/10 (20 rounds)")
		* weapons modified by special equipment (e.g. "LRM20 w/ Artemis IV FSC")
		* CASE protection (e.g. "AC/10 (20 CASE-protected rounds)")
