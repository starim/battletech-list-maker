# frozen_string_literal: true

require_relative "./../importer"

namespace :check do
  desc "Check that Helm Core equipment for all mechs in the database have a valid mapping to database equipment, listing those that do not."
  task :helm_core_equipment_mapping, [:helm_core_fragment_path] => [:environment] do |_t, args|
    helm_core_fragment_path = args.helm_core_fragment_path
    if helm_core_fragment_path.blank?
      raise "Missing required parameter helm_core_fragment_path"
    end

    data_file_path = helm_core_fragment_path + "/json/mechs/"

    unrecognized_equipment = Set.new

    require "json"
    Dir.children(data_file_path).each do |file_name|
      File.open(data_file_path + file_name) do |file|
        data = JSON.load(file)
        begin
          data["weapons"].map do |equipment_data|
            parsed_data = Import.mech_equipment_from_helm_core_name(equipment_data["mtfName"])
            if parsed_data[:name].nil?
              unrecognized_equipment.add(equipment_data["mtfName"])
            end
          end
        end
      end
    end

    if unrecognized_equipment.empty?
      puts "All Helm Core equipment maps to database Equipment models"
    else
      puts "The following Helm Core equipment is missing a mapping to an Equipment model:"
      pp unrecognized_equipment
    end
  end

  desc "Check for MechVariants missing equipment data"
  task mech_variants_missing_equipment: :environment do
    mech_count = MechVariant.count

    mechs_expected_not_to_have_any_equipment = [
      "Celerity CLR-02-X-D",
    ]
    mechs_missing_equipment =
      MechVariant
      .left_outer_joins(:mech_equipment)
      .where(mech_equipment: { id: nil })
      .uniq
      .sort

    mechs_missing_equipment.reject! do |mech|
      mechs_expected_not_to_have_any_equipment.include? mech.full_name
    end

    if mechs_missing_equipment.empty?
      puts "There are no mechs with an empty equipment loadout."
    else
      puts "#{mechs_missing_equipment.length} out of #{mech_count} mechs have some Helm Core data but are missing any equipment:\n"
      puts mechs_missing_equipment.join("\n")
      puts "\n"
    end
  end

  desc "Check for MechChassis whose variants have different tonnages"
  task mech_chassis_tonnage: :environment do
    MechChassis.all.each do |chassis|
      unique_tonnages = chassis.mech_variants.map(&:tonnage).uniq
      next unless unique_tonnages.length > 1

      mech_variant_tonnage_list = chassis.mech_variants.map do |variant|
        "\n#{variant.full_name}: #{variant.tonnage} tons"
      end.join
      puts "#{chassis.name} has variants with different tonnages:#{mech_variant_tonnage_list}"
    end
  end

  desc "Check for MechChassis whose variants have different structure types"
  task mech_chassis_structure: :environment do
    MechChassis.all.each do |chassis|
      unique_structure = chassis.mech_variants.map(&:structure_type).uniq
      next unless unique_structure.length > 1

      mech_variant_structure_list = chassis.mech_variants.map do |variant|
        "\n#{variant.full_name}: #{variant.structure_type} structure"
      end.join
      puts "#{chassis.name} has variants with different structures:#{mech_variant_structure_list}"
    end
  end

  desc "Check for MechChassis which has both Battlemech and Omnimech variants"
  task mech_chassis_mixed_omni: :environment do
    MechChassis.find_each do |chassis|
      unique_omnimech_status = chassis.mech_variants.map(&:omnimech).uniq
      if unique_omnimech_status.length > 1
        list = chassis.mech_variants.map do |variant|
          "\n#{variant.full_name}: #{variant.omnimech ? 'omnimech' : 'battlemech'}"
        end.join
        puts "#{chassis.name} has some Battlemech variants and some Omnimech variants:#{list}"
      end
    end
  end

  desc "Check for MechChassis which has variants with different tech bases"
  task mech_chassis_mixed_tech_base: :environment do
    MechChassis.includes(mech_variants: [:technology_base]).find_each do |chassis|
      unique_tech_bases = chassis.mech_variants.map(&:technology_base_id).uniq
      if unique_tech_bases.length > 1
        list = chassis.mech_variants.map do |variant|
          "\n#{variant.full_name}: #{variant.technology_base.name}"
        end.join
        puts "#{chassis.name} has variants from different technology bases:#{list}"
      end
    end
  end

  desc "Check for anomalies in Equipment data"
  task equipment: :environment do
    equipment_count = Equipment.count

    unused_equipment =
      Equipment
      .all
      .filter { |equipment| equipment.mech_variants.empty? }
      .map(&:name)
      .sort
    unless unused_equipment.empty?
      puts "#{unused_equipment.length} out of #{equipment_count} equipment is never used on any mech:\n"
      puts unused_equipment.join("\n")
      puts "\n"
    end
  end
end
