# frozen_string_literal: true

namespace :export do
  desc "Exports a Helm Core JSON data file for a mech to an MTF file"
  task :helm_core_json_to_mtf, [:json_file_path] => [:environment] do |_t, args|
    json_file_path = args.json_file_path
    raise "Missing required parameter json_file_path" if json_file_path.blank?

    require "json"
    data = nil
    File.open(json_file_path) { |file| data = JSON.parse(file) }
    raise "Empty JSON file given." if data.nil?

    header = <<~HEADER
      Version:1.0
      #{data['name']}
      #{data['model']}
    HEADER

    provenance = <<~PROVENANCE
      Config:#{data['config']}
      TechBase:#{data['techbase']}
      Era:#{data['era']}
      Source:#{data['source']}
      Rules Level:#{data['rules level']}
    PROVENANCE

    chassis = <<~CHASSIS
      Mass:#{data['mass']}
      Engine:#{data['engine']}
      Structure:#{data['structure']}
      Myomer:#{data['myomer']}
    CHASSIS

    movement = <<~MOVEMENT
      Heat Sinks:#{data['heat sinks']}
      Walk MP:#{data['walk mp']}
      Jump MP:#{data['jump mp']}
    MOVEMENT

    armor = <<~ARMOR
      Armor:#{data['armor']}
      LA Armor:#{data['la armor']}
      RA Armor:#{data['ra armor']}
      LT Armor:#{data['lt armor']}
      RT Armor:#{data['rt armor']}
      CT Armor:#{data['ct armor']}
      HD Armor:#{data['hd armor']}
      LL Armor:#{data['ll armor']}
      RL Armor:#{data['rl armor']}
      RTL Armor:#{data['rtl armor']}
      RTR Armor:#{data['rtr armor']}
      RTC Armor:#{data['rtc armor']}
    ARMOR

    weapons = <<~WEAPONS
      Weapons:#{data['weapons'].length}
      #{data['weapons'].map do |weapon|
        weapon_text = []
        weapon['count'].times do
          weapon_text << "#{weapon['mtfName']}, #{weapon['location']}"
        end
        weapon_text.join("\n")
      end.join("\n")}
    WEAPONS

    critical_slots = [
      "left arm",
      "right arm",
      "left torso",
      "right torso",
      "center torso",
      "head",
      "left leg",
      "right leg",
    ].map do |location|
      <<~LOCATION
        #{location.titleize}:
        #{data[location].join("\n")}
      LOCATION
    end.join("\n")

    mtf = [
      header,
      provenance,
      chassis,
      movement,
      armor,
      weapons,
      critical_slots,
    ].join("\n")

    mtf << "\n\n"

    mtf_file_name = "#{data['designation']}.mtf"

    File.write(mtf_file_name, mtf)

    puts "MTF data written to #{mtf_file_name}"
  end
end
