# frozen_string_literal: true

require_relative "./../importer"

namespace :import do
  desc "Import mech data from the Master Unit List"
  task :masterunitlist, [:helm_core_fragment_path] => [:environment] do |_t, args|
    helm_core_fragment_path = args.helm_core_fragment_path
    if helm_core_fragment_path.blank?
      raise "Missing required parameter helm_core_fragment_path"
    end

    helm_core_data_directory = helm_core_fragment_path + "/json/mechs/"

    pages_to_fetch = [
      # Introductory, Standard, and Advanced mechs from eras Star League up to
      # and including the FedCom Civil War
      "http://www.masterunitlist.info/Unit/Filter?Name=&HasBV=true&HasBV=false&MinTons=&MaxTons=&MinBV=&MaxBV=&MinIntro=&MaxIntro=&MinCost=&MaxCost=&HasBFAbility=&MinPV=&MaxPV=&Rules=55&Rules=4&Rules=5&Types=18&BookAuto=&FactionAuto=&AvailableEras=10&AvailableEras=11&AvailableEras=255&AvailableEras=256&AvailableEras=13&AvailableEras=247"
    ]

    errored_mechs = {}

    pages_to_fetch.each do |url|
      page = Import.fetch_page(url)
      raise "Failed to fetch unit list page #{url}" if page.nil?

      page.css("#Tbattlemech table tbody tr").each do |mech_row|
        return_value = Import.import_mech(mech_row, helm_core_data_directory)
        if return_value.is_a?(Hash) && return_value[:error]
          mech_name = return_value[:mech_name]
          error = return_value[:error]
          puts "Failed to import #{mech_name}: #{error}"
          errored_mechs[mech_name] = error
        end
      end
    end

    Import.import_model_availability_for_chassis

    puts "Import complete"
    if !errored_mechs.empty?
      puts "\nThe following mechs errored during import:"
      puts errored_mechs.map { |mech_name, error| "#{mech_name}:\t\t#{error}" }.join("\n")
    end
  end
end
