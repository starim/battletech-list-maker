# frozen_string_literal: true

class SkipMechError < StandardError
  def initialize(mech_name, reason_for_skipping)
    super("Mech #{mech_name} #{reason_for_skipping}. Skipping...")
  end
end

