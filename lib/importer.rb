# frozen_string_literal: true

require "fileutils"
require "json"
require "stringio"
require_relative "./skip_mech_error.rb"

WAIT_SECS_BETWEEN_REQUESTS = 10

module Import
  # fetches a remote HTML page, with caching, and returns a Nokogiri object
  # wrapping the HTML
  #
  # parameters:
  #   * the URL to fetch
  def self.fetch_page(url)
    cache_path = self.fetch_file(url)
    File.open(cache_path) { |f| Nokogiri::HTML(f) }
  end

  # fetches a remote resource, with caching
  #
  # parameters:
  #   * the URL to fetch
  def self.fetch_file(url)
    rails_root = "#{File.expand_path(File.dirname(__FILE__))}/../"

    cache_folder = "#{rails_root}/tmp/importer"
    if !File.exists?(cache_folder)
      FileUtils.mkdir_p(cache_folder)
    end

    protocol_index = url.index("://")
    if protocol_index
      filename = url.split("://", 2).last
    else
      filename = url
    end
    filename.gsub!("/", "-")
    # this limits the filename to 255 characters, though if there are
    # multi-byte Unicode characters in the filename then this can still be too
    # long for some filesystems
    filename = filename.slice(0, 255)

    cache_path = "#{cache_folder}/#{filename}"
    if !File.exists?(cache_path)
      sleep(WAIT_SECS_BETWEEN_REQUESTS)
      URI.open(url) do |page|
        # URI.open yields a StringIO objecti if the fetched content is less
        # than 10 kb, otherwise it yields a File object
        contents = page.is_a?(StringIO) ? page : File.read(page)
        IO.write(cache_path, contents)
      end
    end

    cache_path
  end

  # imports a mech into the database
  #
  # parameters:
  #   * a single row parsed by Nokogiri from the Master Unit List's unit list
  #   page
  #   * a number of seconds to wait sending a request to the Master Unit List
  #   for additional data
  #   * the path to the local Helm Memory Core's JSON data directory
  def self.import_mech(mech_row, helm_core_data_dir)
    mech = MechVariant.new(Import.data_from_unit_list_page(mech_row))

    mul_name = mech_row.css("td")[0].content.strip

    begin
      record_sheet_name = mech_row.css("td")[5].content.strip.split(" ").last
      if record_sheet_name == "None"
        raise SkipMechError.new(mul_name, "is listed as having no record sheet")
      end
      if record_sheet_name == "XTROBoondoggles"
        raise SkipMechError.new(mul_name, "is listed as a failed experiment")
      end

      if Import.on_mech_ignore_list?(mul_name)
        raise SkipMechError.new(
          mul_name,
          "is on ignore list, see comments in on_mech_ignore_list?()",
        )
      elsif mul_name.include? "<Base>"
        raise SkipMechError.new(
          mul_name,
          "is a base Omnimech",
        )
      end

      return unless Import.data_from_helm_core(
        Import.helm_core_file_name(mul_name, helm_core_data_dir),
        mech
      )
      if Import.mech_already_saved?(mech.chassis.name, mech.designation)
        raise SkipMechError.new(mech.full_name, "already exists in the databse")
      end

      detail_page = Import.fetch_page(mech.mul_url)
      if detail_page.nil?
        raise "Failed to fetch unit detail page for #{mech.full_name}"
      end

      mech.assign_attributes(
        Import.data_from_unit_detail_page(detail_page, mech)
      )

      if mech.chassis.sarna_url.nil?
        Import.data_from_sarna(mech)
      end

      if mech.availability.empty?
        raise SkipMechError.new(
          mech.full_name,
          "is listed as Extinct in all relevant eras",
        )
      end

      mech.save!
      puts <<~SAVED_MESSAGE.squish
        Saved #{mech.full_name}: #{mech.tonnage} ton
        #{mech.technology_base.name} #{mech.omnimech ? 'Omnimech' :
        'Battlemech'} introduced #{mech.year_introduced}
      SAVED_MESSAGE

    rescue SkipMechError => reason
      puts reason.message
    rescue => error
      { error: error.message, mech_name: mul_name }
    end
  end

  def self.data_from_unit_list_page(mech_row)
    # unit list columns:
    # 0: mech name and code designation separated by a space
    # 1: icon indicating if the mech is featured in a particular source book
    # 2: tonnage
    # 3: battle value(BV)
    # 4: PV (Alpha Strike points)
    # 5: role
    # 6: list of anchor tags linking to source books that contain the mech
    # 7: rules level: "introductory", "standard", "advanced", "experimental"
    # 8: era icon
    # 9: year introduced
    # 10: button to add the mech to the site's force builder
    columns = mech_row.css("td")

    full_name = columns[0].content.strip

    rules_level_name = columns[7].content.strip
    rules_level = RulesLevel.find_by(name: rules_level_name)
    if rules_level.nil?
      raise SkipMechError.new(
        full_name,
        "has unrecognized rules level #{rules_level_name}",
      )
    end

    {
      full_name: full_name,
      tonnage: columns[2].content.strip.to_i,
      base_battle_value: columns[3].content.gsub(/[^0-9]/, "").to_i,
      rules_level: RulesLevel.find_by(name: rules_level_name),
      year_introduced: columns[9].content.to_i,
      mul_url: "http://masterunitlist.info#{columns[0].at_css('a').attr('href').strip}",
    }
  end

  def self.data_from_unit_detail_page(page, new_mech)
    # detail page rows:
    # 0: tonnage
    # 1: battle value
    # 2: cost
    # 3: rules level
    # 4: technology base
    # 5: type (Battlemech, infantry, vehicle, etc.)
    # 6: role
    # 7: year introduced
    # 8: era introduced
    # 9: notes

    data = {}

    detail_columns =
      page.css(".row .col-sm-8.col-xs-12 .col-sm-5.col-xs-6 .dl-horizontal.dl-large dd")

    page
      .css(".row .col-sm-8.col-xs-12 .col-sm-5.col-xs-6 .dl-horizontal.dl-large dt")
      .map { |label_tag| label_tag.content.strip.downcase }
      .each_with_index do |label, index|
        labels = {
          "cost" => {
            key: :cbill_cost,
            value: ->(text) { text == "NA" ? nil : text.gsub(/[^0-9]/, "").to_i },
          },
          "technology" => {
            key: :technology_base,
            value: ->(text) { TechnologyBase.find_or_initialize_by(name: text) },
          },
          "unit role" => {
            key: :role,
            value: ->(text) { Role.find_or_initialize_by(name: text) },
          },
          "unit type" => {
            key: :omnimech,
            value: ->(text) { /omnimech/i.match?(text) },
          },
        }

        next if labels[label].nil?

        key = labels[label][:key]
        value = labels[label][:value]
        data[key] = value.call(
          detail_columns[index].content.gsub(Nokogiri::HTML("&nbsp;").text, " ").strip
        )
      end

    source_books =
      page
      .xpath("//*[normalize-space() = 'Product Info']/following-sibling::*//a")
      .map { |anchor| anchor.content.strip }
      .compact
      .reject { |name| name == "None" || name == "" }
      .map { |name| SourceBook.find_or_initialize_by(name: name) }

    availability =
      {
        "star-league" => "Star League",
        "early-succession-war" => "Early Succession War",
        "late-succession-war---lostech" => "Late Succession War",
        "late-succession-war---renaissance" => "Late Succession War",
        "clan-invasion" => "Clan Invasion",
        "civil-war" => "FedCom Civil War",
      }.map do |page_id, era|
        [era, page.css("##{page_id} a")]
      end.to_h
      .map do |era, anchors|
        faction_names = anchors.map do |anchor|
          raw_text = anchor.content.strip
          name = raw_text.gsub(Nokogiri::HTML("&nbsp;").text, " ").strip

          # fix typos and variations in spelling in the master unit list
          if name == "Free Rasalhauge Republic"
            "Free Rasalhague Republic"
          elsif name == "Taurian Concordant"
            "Taurian Concordat"
          elsif name == "Clan Wolf (in Exile)"
            "Clan Wolf-in-Exile"
          elsif name == "ComStar"
            "Comstar"
          else
            name
          end
        end

        [era, faction_names]
      end.to_h
      .map do |era, factions|
        universally_ignored_factions = ["Unique"]

        ignored_factions_in_particular_eras = {
          "Star League" => [
          ],
          "Early Succession War" => [
            "Star League in Exile",
            "Clan Diamond Shark",
            "Clan Ghost Bear",
            "Clan Hell's Horses",
            "Clan Jade Falcon",
            "Clan Nova Cat",
            "Clan Sea Fox",
            "Clan Smoke Jaguar",
            "Clan Snow Raven",
            "Clan Wolf",
            "Clan Wolverine",
            "Clan Mongoose",
            "Clan Widowmaker",
          ],
          "Late Succession War" => [
            "Star League in Exile",
            "Clan Diamond Shark",
            "Clan Ghost Bear",
            "Clan Hell's Horses",
            "Clan Jade Falcon",
            "Clan Nova Cat",
            "Clan Sea Fox",
            "Clan Smoke Jaguar",
            "Clan Snow Raven",
            "Clan Wolf",
            "Clan Wolverine",
          ],
          "Clan Invasion" => [
          ],
          "FedCom Civil War" => [
          ],
        }

        [
          era,
          factions.reject do |faction|
            universally_ignored_factions.include?(faction) ||
            ignored_factions_in_particular_eras[era].include?(faction)
          end
        ]
      end.to_h
      .map do |era, factions|
        if factions.include? "Extinct"
          converted_factions = []
        else
          converted_factions =
            factions
            .flat_map do |faction|
              generic_factions = {
                "Star League General" => {
                  "Star League" => [
                    "Star League Regular",
                    "Star League Royal",
                  ],
                },
                "Inner Sphere General" => {
                  "Star League" => [
                    "Capellan Confederation",
                    "Draconis Combine",
                    "Federated Suns",
                    "Free Worlds League",
                    "Lyran Commonwealth",
                  ],
                  "Early Succession War" => [
                    "Capellan Confederation",
                    "Draconis Combine",
                    "Federated Suns",
                    "Free Worlds League",
                    "Lyran Commonwealth",
                    "Comstar",
                  ],
                  "Late Succession War" => [
                    "Capellan Confederation",
                    "Draconis Combine",
                    "Federated Suns",
                    "Free Worlds League",
                    "Lyran Commonwealth",
                    "Free Rasalhague Republic",
                    "St. Ives Compact",
                    "Comstar",
                  ],
                  "Clan Invasion" => [
                    "Capellan Confederation",
                    "Draconis Combine",
                    "Federated Suns",
                    "Free Worlds League",
                    "Lyran Commonwealth",
                    "Comstar",
                    "Federated Commonwealth",
                    "Free Rasalhague Republic",
                    "Lyran Alliance",
                    "St. Ives Compact",
                    "Star League (Second)",
                    "Word of Blake",
                  ],
                  "FedCom Civil War" => [
                    "Capellan Confederation",
                    "Draconis Combine",
                    "Federated Suns",
                    "Free Worlds League",
                    "Comstar",
                    "Federated Commonwealth",
                    "Free Rasalhague Republic",
                    "Lyran Alliance",
                    "St. Ives Compact",
                    "Star League (Second)",
                    "Word of Blake",
                  ],
                },
                "IS Clan General" => {
                  "Early Succession War" => [],
                  "Late Succession War" => [],
                  "Clan Invasion" => [
                    "Clan Diamond Shark",
                    "Clan Ghost Bear",
                    "Clan Jade Falcon",
                    "Clan Nova Cat",
                    "Clan Smoke Jaguar",
                    "Clan Steel Viper",
                    "Clan Wolf",
                    "Clan Wolf-in-Exile",
                  ],
                  "FedCom Civil War" => [
                    "Clan Diamond Shark",
                    "Clan Ghost Bear",
                    "Clan Hell's Horses",
                    "Clan Jade Falcon",
                    "Clan Nova Cat",
                    "Clan Wolf",
                    "Clan Wolf-in-Exile",
                  ],
                },
                "HW Clan General" => {
                  "Early Succession War" => [],
                  "Late Succession War" => [],
                  "Clan Invasion" => [
                    "Clan Blood Spirit",
                    "Clan Burrock",
                    "Clan Cloud Cobra",
                    "Clan Coyote",
                    "Clan Fire Mandrill",
                    "Clan Goliath Scorpion",
                    "Clan Hell's Horses",
                    "Clan Ice Hellion",
                    "Clan Snow Raven",
                    "Clan Star Adder",
                  ],
                  "FedCom Civil War" => [
                    "Clan Blood Spirit",
                    "Clan Burrock",
                    "Clan Cloud Cobra",
                    "Clan Coyote",
                    "Clan Fire Mandrill",
                    "Clan Goliath Scorpion",
                    "Clan Ice Hellion",
                    "Clan Snow Raven",
                    "Clan Star Adder",
                    "Clan Steel Viper",
                  ],
                },
                "Periphery General" => {
                  "Star League" => [
                    "Circinus Federation",
                    "Magistracy of Canopus",
                    "Marian Hegemony",
                    "Outworlds Alliance",
                    "Pirates",
                    "Taurian Concordat",
                  ],
                  "Early Succession War" => [
                    "Circinus Federation",
                    "Magistracy of Canopus",
                    "Marian Hegemony",
                    "Outworlds Alliance",
                    "Pirates",
                    "Taurian Concordat",
                  ],
                  "Late Succession War" => [
                    "Circinus Federation",
                    "Magistracy of Canopus",
                    "Marian Hegemony",
                    "Outworlds Alliance",
                    "Pirates",
                    "Taurian Concordat",
                  ],
                  "Clan Invasion" => [
                    "Circinus Federation",
                    "Magistracy of Canopus",
                    "Marian Hegemony",
                    "Outworlds Alliance",
                    "Pirates",
                    "Taurian Concordat",
                  ],
                  "FedCom Civil War" => [
                    "Circinus Federation",
                    "Magistracy of Canopus",
                    "Marian Hegemony",
                    "Outworlds Alliance",
                    "Pirates",
                    "Taurian Concordat",
                  ],
                },
              }

              if generic_factions.keys.include? faction
                factions = generic_factions[faction][era]
                if factions.nil?
                  raise "No entry for era \"#{era}\" in factions list for \"#{faction}\""
                end

                factions
              else
                faction
              end
            end
        end

        [era, converted_factions]
      end.to_h
      .map { |era, factions| [era, factions.uniq] }
      .reject { |_era, factions| factions.empty? }.to_h
      .flat_map do |era_name, faction_names|
        faction_names.map do |faction_name|
          faction = Faction.find_by(name: faction_name)
          era = Era.find_by(name: era_name)

          if faction.nil?
            raise "Unrecognized faction: #{faction_name} for era #{era_name}"
          end
          if era.nil?
            raise "Unrecognized era: #{era_name}"
          end

          Availability.new(
            mech_variant: new_mech,
            era: era,
            faction: faction,
          )
        end
      end

    battle_values = []
    page
      .xpath("//*[normalize-space() = 'Skills']/following-sibling::table//table//tbody//tr")
      .each_with_index do |row, gunnery_skill|
        row.xpath("td").each_with_index do |td, piloting_skill|
          battle_value = td.content.gsub(/[^0-9]/, "").to_i
          battle_values << {
            gunnery_skill: gunnery_skill,
            piloting_skill: piloting_skill,
            battle_value: battle_value,
          }
        end
      end
    battle_values.map! do |data|
      BattleValue.new(
        mech_variant: new_mech,
        piloting_skill: data[:piloting_skill],
        gunnery_skill: data[:gunnery_skill],
        battle_value: data[:battle_value]
      )
    end

    data.merge(
      source_books: source_books,
      availability: availability,
      battle_values: battle_values
    )
  end

  def self.data_from_helm_core(json_file_name, mech)
    File.open(json_file_name) do |file|
      data = JSON.load(file)
      if data["config"]
        is_biped = /biped/i.match?(data["config"])
        return false unless is_biped
      end

      chassis_name = data["name"].strip
      clan_chassis_name = data["alias"]&.strip
      # special cases where the alias field isn't actually a Clan name
      if(
        (data["name"] == "Wolf Trap" && clan_chassis_name == "Tora") ||
        (data["name"] == "Von Rohrs" && clan_chassis_name == "Hebi") ||
        (data["name"] == "Katana" && clan_chassis_name == "Crockett")
      )
        clan_chassis_name = nil
      end
      if data["model"].split(" ").length > 1
        model_number = data["model"].split(" ")[0]
        nickname = data["model"].split(" ")[1].tr("()", '"')
        designation = "#{model_number} #{nickname}"
      else
        designation = data["model"]
      end

      mech.chassis = MechChassis.where(
        name: chassis_name, clan_name: clan_chassis_name
      ).first_or_initialize
      mech.designation = designation

      if data["myomer"] != "Standard"
        name = case data["myomer"]
        when "Triple Strength Myomer"
          "Triple-Strength Myomer"
        when "Triple-Strength"
          "Triple-Strength Myomer"
        when "MASC"
          "MASC"
        when "ISMASC"
          "MASC"
        when "CLMASC"
          "Clan MASC"
        else
          raise "New myomer value \"#{data['myomer']}\""
        end

        special_myomer = MechEquipment.where(
          mech_variant: mech,
          equipment: Equipment.find_by(name: name)
        ).first_or_initialize
        special_myomer.count = 1
        mech.mech_equipment << special_myomer
      end

      {
        "walk mp" => "walking_mp",
        "jump mp" => "jumping_mp",
        "armor" => "armor_type",
      }.each do |helm_name, model_attribute_name|
        mech.send("#{model_attribute_name}=", data[helm_name])
      end

      mech.heat_sinks = data["heat sinks"].downcase

      mech.engine_model =
        data["engine"]
        .gsub(/fusion/i, "")
        .gsub(/engine/i, "")
        .gsub(/\(Clan\)/i, "")
        .gsub(/\(IS\)/i, "")
        .strip
        .gsub(/\s+/, " ")

      mech.structure_type =
        data["structure"]
        .gsub(/IS/i, "")
        .gsub(/clan/i, "")
        .gsub(/-/, " ")
        .strip
        .gsub(/\s+/, " ")

      # armor is summed over each section
      begin
        mech.front_armor =
          %w[la ra lt rt ct hd ll rl]
          .map { |section| data["#{section} armor"].strip.to_i }.sum
      rescue StandardError
        # the Helm Core Fragment is inconsistent about keys for armor
        # locations in non-bipedal mechs, so I attempt to use the bipedal
        # armor keys first, then fall back to non-bipedal armor keys
        mech.front_armor =
          %w[fll frl lt rt ct hd rll rrl]
          .map { |section| data["#{section} armor"].strip.to_i }.sum
      end
      mech.rear_armor =
        %w[rtl rtr rtc]
        .map { |section| data["#{section} armor"].strip.to_i }.sum

      data["weapons"].map do |equipment_data|
        count = equipment_data["count"]
        parsed_data = Import.mech_equipment_from_helm_core_name(equipment_data["mtfName"])
        if parsed_data[:name].nil?
          raise "Unknown equipment in Helm Memory Fragment: \"#{equipment_data['mtfName']}\". This equipment is missing a mapping to a database-recognized Equipment name."
        end

        {
          name: parsed_data[:name],
          count: count,
          rear_facing: parsed_data[:rear_facing],
        }
      end
                     .group_by { |data| { name: data[:name], rear_facing: data[:rear_facing] } }
                     .map { |key, entries| [key, entries.map { |entry| entry[:count] }.sum] }.to_h
                     .each do |key, count|
        name = key[:name]
        rear_facing = key[:rear_facing]
        equipment = Equipment.find_by(name: name)
        raise "No equipment entry in database for \"#{name}\"" if equipment.nil?

        mech_equipment = MechEquipment.find_or_initialize_by(
          mech_variant: mech,
          equipment: equipment,
          rear_facing: rear_facing
        )
        mech_equipment.count = count
        mech.mech_equipment << mech_equipment
      end
    end
    true
  end

  def self.data_from_sarna(mech)
    special_case_urls = {
      "Nexus II" => "https://www.sarna.net/wiki/Nexus",
    }

    complete_url = -> (url) {
      if url.nil?
        nil
      elsif url.starts_with?("//")
        "https:#{url}"
      elsif url.starts_with?("/")
        "https://www.sarna.net#{url}"
      else
        url
      end
    }

    if special_case_urls.keys.include? mech.chassis.name
      sarna_url = special_case_urls[mech.chassis.name]
    else
      if mech.chassis.clan_name && mech.chassis.clan_name != mech.chassis.name
        sarna_page_name = "#{mech.chassis.clan_name} (#{mech.chassis.name})"
      else
        sarna_page_name = mech.chassis.name
      end
      sarna_url = "https://www.sarna.net/wiki/#{sarna_page_name.gsub(" ", "_")}"
    end
    begin
      sarna_page = Import.fetch_page(sarna_url)
    rescue
      puts "Failed to fetch Sarna page for #{mech.full_name} using URL #{sarna_url}"
      raise
    end
    title = sarna_page.css("title").first.content.strip
    if /disambiguation/.match?(title) || sarna_page.css(".infobox a.image").first.nil?
      sarna_url += mech.omnimech ? "_(OmniMech)" : "_(BattleMech)"
      begin
        sarna_page = Import.fetch_page(sarna_url)
      rescue
        puts "Failed to fetch Sarna page for #{mech.full_name} using URL #{sarna_url}"
        raise
      end
    end
    mech.chassis.sarna_url = sarna_url

    special_case_images = {
      "Hoplite" => "https://cfw.sarna.net/wiki/images/archive/8/8d/20110118183644%213050U_Hoplite.jpg?timestamp=20100619101752",
      "Verfolger" => "https://cfw.sarna.net/wiki/images/archive/2/25/20150201204910%21Verfolger.gif?timestamp=20070508033717",

      # Sarna likes to use an orthographic schematic view as the thumbnail for
      # Clan mechs, but I think these don't convey the look of the mech very
      # well.
      "Vulture" => "https://cfw.sarna.net/wiki/images/thumb/4/4e/Mad_Dog.jpg/462px-p35tfos40cio9v2kctq8s12rmn5x0rk.png?timestamp=20110118185926",
      "Mad Cat" => "https://cfw.sarna.net/wiki/images/thumb/2/29/MadCat_2.jpeg/458px-kh24o41mw058om2z49ocd0y38fqcflt.jpg?timestamp=20150222182908",
      "Uller" => "https://cfw.sarna.net/wiki/images/thumb/1/1d/Uller.png/622px-rl00ulxmopjzrnyyk1ceaijfp6jhmtf.png?timestamp=20110118185515",
      "Loki" => "https://cfw.sarna.net/wiki/images/thumb/4/44/Loki3050.JPG/522px-nyzzwuqiyypbmag2koyzmz6rle98w5m.jpg?timestamp=20100214153052",
      "Daishi" => "https://cfw.sarna.net/wiki/images/thumb/8/89/DireWolf.jpg/455px-4vm2e4mmr5chhiqxsfeln0lutcovjmr.jpg?timestamp=20150222182347",
      "Koshi" => "https://cfw.sarna.net/wiki/images/thumb/8/83/3050U_Koshi.jpg/460px-4rip1cl3ewe40ajkh8qs0xwbjr0o8nt.png?timestamp=20110118185325",
      "Ryoken" => "https://cfw.sarna.net/wiki/images/thumb/e/e9/Ryoken_2.jpg/454px-trhaqfal3d3oigmsyvlxizxqelv0xzb.jpg?timestamp=20150222183145",
      "Puma" => "https://cfw.sarna.net/wiki/images/thumb/b/bc/3050U_Adder.jpg/520px-0ktlwoke53602cxtidqw6up8nqxpjwr.png?timestamp=20110118185611",
      "Masakari" => "https://cfw.sarna.net/wiki/images/thumb/b/bd/3050U_Masakari.jpg/566px-lbe2ath6ofqv8ua8xwlkt1wdkv0royn.png?timestamp=20110118190401",
      "Man O' War" => "https://cfw.sarna.net/wiki/images/thumb/8/88/Gargolye.png/585px-46eifje1log322i1q5a7w399g3nzcs2.png?timestamp=20110118190258",
      "Fenris" => "https://cfw.sarna.net/wiki/images/thumb/4/46/3050U_Ice_Ferret.jpg/447px-c5bh1uaxbqx3n1mz24nupwnw3i8bxn7.png?timestamp=20110118185741",
      "Dragonfly" => "https://cfw.sarna.net/wiki/images/thumb/c/c6/Dragonfly_2.jpg/626px-ok8tq5f9hs9n0rzad99w99jng0ieguh.jpg?timestamp=20150222183710",
      "Dasher" => "https://cfw.sarna.net/wiki/images/thumb/2/2d/3050U_Dasher.jpg/554px-r1o83ta3fjoqtn6ui98scvuuk4rwqyf.png?timestamp=20110118185234",
      "Black Hawk" => "https://cfw.sarna.net/wiki/images/thumb/d/de/3050U_Nova.jpg/782px-5zlfvnehco98u636g833xl6gkxvwhst.png?timestamp=20110118185821",
    }

    if special_case_images.keys.include?(mech.chassis.name)
      image_url = special_case_images[mech.chassis.name]
      image_path = Import.fetch_file(image_url)
    else
      begin
        full_image_page_link = sarna_page.css(".infobox a.image").first
      rescue
        puts "Couldn't find a full image link for mech #{mech.chassis.name} at #{sarna_url}"
        raise
      end
      begin
        full_image_page_url = complete_url.call(full_image_page_link.attr("href"))
      rescue
        puts "Couldn't find a full image page for the #{mech.chassis.name}"
        raise
      end

      begin
        image_page = Import.fetch_page(full_image_page_url)
      rescue
        puts "Failed to fetch full image page for #{mech.full_name}"
        raise
      end
      image_url = complete_url.call(image_page.css("#file img").first.attr("src"))
      begin
        image_path = Import.fetch_file(image_url)
      rescue
        # sometimes the full image thumbail doesn't exist so we have to grab the
        # full image
        image_url = complete_url.call(image_page.css("#file a").first.attr("href"))
        image_path = Import.fetch_file(image_url)
      end
    end

    mech.chassis.image.attach(
      io: File.open(image_path, "rb"), filename: mech.chassis.name
    )
  end

  def self.on_mech_ignore_list?(mul_name)
    ignored_mechs = [
      "Crusader CRD-4L",    # identical to CRD-4D and uses its record sheet
      "Victor VTR-9D",      # identical to VTR-9K and uses its record sheet
      "Flea FLE-16",        # doesn't currently have a Helm Core entry

      # The Master Unit List lists this mech as being in Record Sheets 3050
      # upgrades but neither Sarna nor the Helm Core contains any information.
      "Wolfhound (Lucian Finn)",

      "Hyena HYN-KTO",      # an Industrialmech and not a Battlemech

      # This mech is listed as an apocryphal MWO-only mech by Sarna, but the
      # Master Unit List claims it has a record sheet in Record Sheets: 3067
      # Unabridged. The Helm Core Fragment has no data on it.
      "Thanatos TNS-4S",

      "Epimetheus Prime",   # this is a prototype and not a production mech

      # The Master Unit List claims this mech's record sheet is in Record
      # Sheets 3050 Upgrade Unabridged but neither Sarna nor the Helm Core
      # Fragment has any reference to this mech.
      "Banshee (John Bauer)",

      # According to the Master Unit List the record sheet for this mech exists
      # in Historical Turning Points Mallory's World but the Helm Core Fragment
      # has no data on it.
      "Archer ARC-2R (Morgan)",

      # According to the Master Unit List the record sheet for this mech exists
      # in Historical Turning Points Mallory's World but the Helm Core Fragment
      # has no data on it.
      "Warhammer WHM-6K (Yorinaga)",

      # The Master Unit List shows this mech as available under standard rules
      # level in the Civil War era, but it uses experimental weapons like Heavy
      # PPCs for that era. Sarna's history for this variant lists it as an
      # experimental testbed.
      "Zeus-X ZEU-9WD",

      # Sarna lists this as a prototype that never reached full production
      "Titan TI-1A",

      # Sarna lists the Rifleman II as a prototype that never reached full
      # production
      "Rifleman II RFL-3N-2",
      "Rifleman II RFL-3N-2 (Kataga)",
    ]

    ignored_mechs.include? mul_name
  end

  def self.mech_equipment_from_helm_core_name(helm_core_name)
    helm_core_name.strip!
    if helm_core_name.include?("(R)") || helm_core_name.include?("(Rear)")
      helm_core_name.gsub!(/\(R\)/, "")
      helm_core_name.gsub!(/\(Rear\)/, "")
      helm_core_name.strip!
      rear_facing = true
    else
      rear_facing = false
    end

    name = if Equipment.where(name: helm_core_name).count > 0
      helm_core_name
    else
      decode_helm_core_equipment_name(helm_core_name)
    end

    { name: name, rear_facing: rear_facing }
  end

  def self.decode_helm_core_equipment_name(helm_core_name)
    name = case helm_core_name

    # autocannons
    when "ISAC2"
      "AC/2"
    when "ISAC5"
      "AC/5"
    when "ISAC10"
      "AC/10"
    when "ISAC20"
      "AC/20"
    when "Autocannon/2"
      "AC/2"
    when "Autocannon/5"
      "AC/5"
    when "Autocannon/10"
      "AC/10"
    when "Autocannon/20"
      "AC/20"
    when "ISUltraAC2"
      "Ultra AC/2"
    when "ISUltraAC5"
      "Ultra AC/5"
    when "ISUltraAC10"
      "Ultra AC/10"
    when "ISUltraAC20"
      "Ultra AC/20"
    when "CLUltraAC2"
      "Clan Ultra AC/2"
    when "CLUltraAC5"
      "Clan Ultra AC/5"
    when "CLUltraAC10"
      "Clan Ultra AC/10"
    when "CLUltraAC20"
      "Clan Ultra AC/20"
    when "ISLBXAC2"
      "LBX AC/2"
    when "ISLBXAC5"
      "LBX AC/5"
    when "ISLBXAC10"
      "LBX AC/10"
    when "ISLBXAC10Prototype"
      "LBX AC/10"
    when "ISLBXAC20"
      "LBX AC/20"
    when "CLLBXAC2"
      "Clan LBX AC/2"
    when "CLLBXAC5"
      "Clan LBX AC/5"
    when "CLLBXAC10"
      "Clan LBX AC/10"
    when "CLLBXAC20"
      "Clan LBX AC/20"
    when "LB 2-X AC"
      "LBX AC/2"
    when "LB 5-X AC"
      "LBX AC/5"
    when "LB 10-X AC"
      "LBX AC/10"
    when "LB 20-X AC"
      "LBX AC/20"
    when "ISRotaryAC2"
      "RAC/2"
    when "ISRotaryAC5"
      "RAC/5"
    when "Rotary AC/2"
      "RAC/2"
    when "Rotary AC/5"
      "RAC/5"
    when "ISLAC2"
      "Light AC/2"
    when "ISLAC5"
      "Light AC/5"
    when "Light Auto Cannon/5"
      "Light AC/5"

    # gauss rifles
    when "ISGaussRiflePrototype"
      "Gauss Rifle"
    when "ISGaussRifle"
      "Gauss Rifle"
    when "CLGaussRifle"
      "Clan Gauss Rifle"
    when "ISLightGaussRifle"
      "Light Gauss Rifle"
    when "CLLightGaussRifle"
      "Clan Light Gauss Rifle"
    when "ISHeavyGaussRifle"
      "Heavy Gauss Rifle"
    when "HAG/20"
      "Hyper-Assault Gauss Rifle 20"
    when "HAG/30"
      "Hyper-Assault Gauss Rifle 30"
    when "HAG/40"
      "Hyper-Assault Gauss Rifle 40"
    when "CLHAG20"
      "Clan Hyper-Assault Gauss Rifle 20"
    when "CLHAG30"
      "Clan Hyper-Assault Gauss Rifle 30"
    when "CLHAG40"
      "Clan Hyper-Assault Gauss Rifle 40"
    when "CLAPGaussRifle"
      "Clan Anti-Personnel Gauss Rifle"

    # machine guns
    when "ISMachine Gun"
      "Machine Gun"
    when "ISLightMG"
      "Light Machine Gun"
    when "CLMG"
      "Clan Light Machine Gun"
    when "CLHeavyMG"
      "Clan Heavy Machine Gun"
    when "CLLightMG"
      "Clan Light Machine Gun"
    when "ISLMGA"
      "Light Machine Gun Array"
    when "CLLMGA"
      "Clan Light Machine Gun Array"
    when "CLHMGA"
      "Clan Heavy Machine Gun Array"

    # flamers
    when "ISFlamer"
      "Flamer"
    when "CLFlamer"
      "Clan Flamer"

    # lasers
    when "ISSmallLaser"
      "Small Laser"
    when "ISERSmallLaser"
      "ER Small Laser"
    when "ISSmallXPulseLaser"
      "Small X-Pulse Laser"
    when "CLERSmallLaser"
      "Clan ER Small Laser"
    when "ISMediumLaser"
      "Medium Laser"
    when "ISERMediumLaser"
      "ER Medium Laser"
    when "CLERMediumLaser"
      "Clan ER Medium Laser"
    when "ISSmallPulseLaser"
      "Small Pulse Laser"
    when "CLSmallPulseLaser"
      "Clan Small Pulse Laser"
    when "CLMicroPulseLaser"
      "Clan Micro Pulse Laser"
    when "CLERMicroLaser"
      "Clan ER Micro Laser"
    when "ISMediumPulseLaser"
      "Medium Pulse Laser"
    when "ISMediumPulseLaserPrototype"
      "Medium Pulse Laser"
    when "CLMediumPulseLaser"
      "Clan Medium Pulse Laser"
    when "ISLargeLaser"
      "Large Laser"
    when "ER Large Laser Prototype"
      "ER Large Laser"
    when "CLLargeLaser"
      "Clan Large Laser"
    when "ISERLargeLaser"
      "ER Large Laser"
    when "CLLargePulseLaser"
      "Clan Large Pulse Laser"
    when "CLERLargeLaser"
      "Clan ER Large Laser"
    when "ISLargePulseLaser"
      "Large Pulse Laser"
    when "CLHeavySmallLaser"
      "Clan Heavy Small Laser"
    when "CLHeavyMediumLaser"
      "Clan Heavy Medium Laser"
    when "CLHeavyLargeLaser"
      "Clan Heavy Large Laser"
    when "ISLargeXPulseLaser"
      "Large X-Pulse Laser"

    # PPCs
    when "ISPPC"
      "PPC"
    when "Particle Cannon"
      "PPC"
    when "ISERPPC"
      "ER PPC"
    when "CLERPPC"
      "Clan ER PPC"
    when "ISSNPPC"
      "Snub-Nose PPC"
    when "ISLPPC"
      "Light PPC"
    when "ISHeavyPPC"
      "Heavy PPC"
    when "ISHPPC"
      "Heavy PPC"

    # misc. energy weapons
    when "ISPlasmaRifle"
      "Plasma Rifle"
    when "CLPlasmaCannon"
      "Clan Plasma Cannon"

    # missiles
    when "ISSRM2"
      "SRM 2"
    when "ISSRM4"
      "SRM 4"
    when "ISSRM6"
      "SRM 6"
    when "CLSRM2"
      "Clan SRM 2"
    when "CLSRM4"
      "Clan SRM 4"
    when "CLSRM6"
      "Clan SRM 6"
    when "ISStreakSRM2"
      "Streak SRM 2"
    when "ISStreakSRM4"
      "Streak SRM 4"
    when "ISStreakSRM6"
      "Streak SRM 6"
    when "CLStreakSRM2"
      "Clan Streak SRM 2"
    when "CLStreakSRM4"
      "Clan Streak SRM 4"
    when "CLStreakSRM6"
      "Clan Streak SRM 6"
    when "CLStreakSRM6Prototype"
      "Clan Streak SRM 6"
    when "ISSRM4 (OS)"
      "One-Shot SRM 4"
    when "Streak SRM 4 (OS)"
      "One-Shot Streak SRM 4"
    when "CLStreakSRM4 (OS)"
      "One-Shot Clan Streak SRM 4"
    when "Streak SRM 2 (OS)"
      "One-Shot Streak SRM 2"
    when "ISMRM10"
      "MRM 10"
    when "ISMRM20"
      "MRM 20"
    when "ISMRM30"
      "MRM 30"
    when "ISMRM40"
      "MRM 40"
    when "ISLRM5"
      "LRM 5"
    when "ISLRM10"
      "LRM 10"
    when "ISLRM15"
      "LRM 15"
    when "ISLRM20"
      "LRM 20"
    when "CLLRM5"
      "Clan LRM 5"
    when "CLLRM10"
      "Clan LRM 10"
    when "CLLRM15"
      "Clan LRM 15"
    when "CLLRM20"
      "Clan LRM 20"
    when "ISRocketLauncher10"
      "Rocket Launcher 10"
    when "ISRocketLauncher15"
      "Rocket Launcher 15"
    when "ISRocketLauncher20"
      "Rocket Launcher 20"
    when "CLATM3"
      "Clan ATM 3"
    when "CLATM6"
      "Clan ATM 6"
    when "CLATM9"
      "Clan ATM 9"
    when "CLATM12"
      "Clan ATM 12"
    when "ISArrowIVSystem"
      "Arrow IV"
    when "CLArrowIVSystem"
      "Clan Arrow IV"
    when "ISMML3"
      "Multi-Missile Launcher 3"
    when "ISMML5"
      "Multi-Missile Launcher 5"
    when "ISMML7"
      "Multi-Missile Launcher 7"
    when "ISMML9"
      "Multi-Missile Launcher 9"
    when "UnknownCritical(0xda)"
      "MRM 20"

    # artillery
    when "Sniper"
      "Sniper Artillery"

    # misc. equipment
    when "ISGuardianECM"
      "Guardian ECM"
    when "CLECMSuite"
      "Clan ECM"
    when "ISBeagleActiveProbe"
      "Beagle Active Probe"
    when "CLActiveProbe"
      "Clan Active Probe"
    when "CLLightActiveProbe"
      "Clan Light Active Probe"
    when "ISTAG"
      "TAG"
    when "CLTAG"
      "Clan TAG"
    when "CLLightTAG"
      "Clan Light TAG"
    when "Narc"
      "Narc Beacon"
    when "ISNarcBeacon"
      "Narc Beacon"
    when "CLNarcBeacon"
      "Clan Narc Beacon"
    when "iNarc"
      "Improved Narc Beacon"
    when "ISImprovedNarc"
      "Improved Narc Beacon"
    when "ISC3MasterComputer"
      "C3 Command Unit"
    when "C3 Master with TAG"
      "C3 Command Unit with TAG"
    when "ISC3SlaveUnit"
      "C3 Slave Unit"
    when "ISImprovedC3CPU"
      "C3i Unit"
    when "ISAntiMissileSystem"
      "AMS"
    when "CLAntiMissileSystem"
      "Clan AMS"
    when "Laser AMS"
      "LAMS"
    when "CLAntiPersonnelPod"
      "A-Pod"

    end

    if name.present? && Equipment.where(name: name).count == 0
      raise "A mapping is present for equipment name \"#{name}\" but there is no Equipment model with that name. You should ensure this equipment name is in the seeds file and then re-seed the database."
    end

    name
  end

  def self.mech_already_saved?(chassis_name, designation)
    MechVariant
      .joins(:chassis)
      .where(
        mech_chassis: { name: chassis_name },
        designation: designation
      ).count > 0
  end

  # guesses the Helm Core file name for a mech's data from the name given by
  # the Master Unit List
  def self.helm_core_file_name(mul_name, helm_core_data_dir)
    helm_core_file_name =
      mul_name.gsub("(Standard)", "").tr('"', "").strip.to_s

    special_case_names = {
      "Baboon (Howler) 3 Devil" => "Baboon (Howler) 3",
      "Mongoose Gunslinger MON-66GX" => "Mongoose MON-66GX Gunslinger",
      "Tarantula ZPH-1" => "Tarantula ZPH-1A",
      "Daimyo DMO-1K2 (al-Shahab)" => "Daimyo DMO-1K2 (Al-Shahab)",
      "Hermes II HER-5ME Mercury Elite" => "Hermes II HER-5ME",
      "Wyvern City WVE-5UX" => "Wyvern WVE-5UX City",
      "Centurion CN9-YLW2 Yen Lo Wang" => "Centurion CN9-YLW2",
      "Exterminator EXT-4DX" => "Exterminator EXT-4DX Caine",
      "Tempest TMP-3M2 Storm Tempest" => "Tempest TMP-3M2_Storm_Tempest",
      "Marauder (Bounty Hunter-3015)" => "Marauder Bounty Hunter 3015",
      "Marauder (Bounty Hunter-3044)" => "Marauder Bounty Hunter 3044",
      "Thunder Stallion 2 Fire Stallion" => "Thunder Stallion 2",
      "Jackrabbit JKR-9R Joker" => "Jackrabbit JKR-9R",
      "Hammer HMR-3C Claw-Hammer" => "Hammer HMR-3C",
      "Hammer HMR-3P Pein-Hammer" => "Hammer HMR-3P",
      "Hammer HMR-3S Slammer" => "Hammer HMR-3S",
      "Phoenix Hawk PXH-1b Special" => "Phoenix Hawk PXH-1b (Special)",
      "Phoenix Hawk PXH-1c Special" => "Phoenix Hawk PXH-1c (Special)",
      "Wolf Trap (Tora) WFT-1 (Daitama)" => "Wolf Trap WFT-1 (Daitama)",
      "Lineholder KW1-LH8 Linebreaker" => "Lineholder KW1-LH8",
      "Catapult CPLT-C1 (Jenny) Butterbee" => "Catapult CPLT-C1 Jenny",
      "Mad Cat (Timber Wolf) (Bounty Hunter)" => "Mad Cat Bounty Hunter",
      "Charger CGR-SB Challenger" => "Charger CGR-SB",
      "Battlemaster BLR-1G (Red Corsair)" => "BattleMaster BLR-1G (Red Corsair)",
      "Roadrunner (Emerald Harrier) RD-1R" => "Emerald Harrier (Roadrunner) RD-1R",

      # There is a PR open on the Helm Core Fragment to correct these
      # inconsistencies. Uncomment this section if using the un-updated
      # original Helm Core Fragment.
      # "Von Rohrs (Hebi) VON 4RH-5" => "Von Rohrs (Hebis) VON 4RH-5",
      # "JagerMech JM6-DG" => "Jagermech JM6-DG",
      # "Grendel (Mongrel) Prime" => "Grendel Prime",
      "Ti Ts'ang China Doll" => "Ti Ts'ang TSG-9 China Doll",
    }
    if special_case_names[helm_core_file_name]
      return "#{helm_core_data_dir}/#{special_case_names[helm_core_file_name]}.json"
    end

    if !Pathname.new("#{helm_core_data_dir}/#{helm_core_file_name}.json").exist? &&
       helm_core_file_name.end_with?(")")

      # there are a lot of mechs with a Master Unit List name that has a
      # nickname in parenthesis but in the Helm Memory Core the parenthesis are
      # omitted
      last_open_paren = helm_core_file_name.rindex("(")
      alt_name = helm_core_file_name.dup
      alt_name.slice!(last_open_paren)
      alt_name.delete_suffix!(")")
      if Pathname.new("#{helm_core_data_dir}/#{alt_name}.json").exist?
        return "#{helm_core_data_dir}/#{alt_name}.json"
      end
    end

    "#{helm_core_data_dir}/#{helm_core_file_name}.json"
  end

  # imports all model availability data for all chassis
  def self.import_model_availability_for_chassis
    mechs_in_succession_wars_boxes = [
      "Locust",
      "Commando",
      "Shadow Hawk",
      "Griffin",
      "Wolverine",
      "Catapult",
      "Thunderbolt",
      "Awesome",
      "BattleMaster",
    ]
    mechs_in_succession_wars_boxes.each do |chassis_name|
      chassis = MechChassis.find_by(name: chassis_name)
      if chassis.nil?
        raise "Couldn't find mech chassis named \"#{chassis_name}\"."
      end

      chassis.plastic_model_release_date = Date.new(2019, 1, 31)
      chassis.plastic_model_owned = true
      chassis.save!
    end

    mechs_bought_in_clan_invasion_kickstarter = [
      "Gladiator", "Mad Cat", "Black Hawk", "Grendel", "Puma", # Clan Invasion box
      "Man O' War", "Loki", "Vulture", "Fenris", "Dragonfly",  # Clan heavy striker star
      "Daishi", "Thor", "Ryoken", "Shadow Cat", "Koshi", # Clan command star
      "Marauder", "Archer", "Valkyrie", "Stinger", # Inner Sphere command lance
      "Warhammer", "Rifleman", "Phoenix Hawk", "Wasp", # Inner Sphere battle lance
      "King Crab", "Highlander", "Black Knight", "Exterminator", "Sentinel", "Mercury", # Comstar command level II
      "Masakari", "Nova Cat", "Cougar", "Uller", "Dasher", # Clan fire star
      "Atlas", "Atlas II", "Marauder II", "Orion", "Crusader", # Inner Sphere direct fire lance
      "Banshee", "Grasshopper", "Centurion", "Hatchetman", # Inner Sphere heavy lance
      "Blackjack", "Panther", "Wolfhound", "Jenner", # Inner Sphere striker lance
      "Nightstar", "Cataphract", "Axman", "Bushwacker", # Inner Sphere heavy battle lance
      "Victor", "Enforcer", "Hunchback", "Raven", # Inner Sphere urban lance
      "Behemoth", "Supernova", "Marauder IIC", "Warhammer IIC", "Hunchback IIC", # Clan Heavy Star
      "Turkina", "Kingfisher", "Cauldron-Born", "Crossbow", "Nobori-nin", # Clan Heavy Battle Star
      "Stalker", "Longbow", "Zeus", "Trebuchet", # Inner Sphere fire lance
      "Cyclops", "Thug", "Dragon", "Spider", # Inner Sphere support lance
      "Goshawk", "Hellhound", "Peregrine", "Vixen", "Piranha", # Clan striker star
      "Night Gyr", "Linebacker", "Black Lanner", "Battle Cobra", "Hankyu", # Clan support star
      "Kodiak", "Fire Falcon", "Hellion", "Baboon", "Pack Hunter", # Clan ad-hoc star
    ]

    mechs_in_clan_invasion_wave1 = [
      "Gladiator", "Mad Cat", "Black Hawk", "Grendel", "Puma", # Clan Invasion box
      "Daishi", "Thor", "Ryoken", "Shadow Cat", "Koshi", # Clan command star
      "Man O' War", "Loki", "Vulture", "Fenris", "Dragonfly", # Clan heavy striker star
      "Marauder", "Archer", "Valkyrie", "Stinger", # Inner Sphere command lance
      "Warhammer", "Rifleman", "Phoenix Hawk", "Wasp", # Inner Sphere battle lance
      "UrbanMech",    # one-off?
    ]
    mechs_in_clan_invasion_wave1.each do |chassis_name|
      chassis = MechChassis.find_by(name: chassis_name)
      if chassis.nil?
        raise "Couldn't find mech chassis named \"#{chassis_name}\"."
      end

      if mechs_bought_in_clan_invasion_kickstarter.include?(chassis.name)
        chassis.plastic_model_owned = true
      end
      chassis.plastic_model_release_date = Date.new(2020, 8, 1)
      chassis.save!
    end

    mechs_in_clan_invasion_wave2 = [
      "Goshawk", "Hellhound", "Peregrine", "Vixen", "Piranha", # Clan striker star
      "Atlas", "Atlas II", "Marauder II", "Orion", "Crusader", # Inner Sphere direct fire lance
      "King Crab", "Highlander", "Black Knight", "Exterminator", "Sentinel", "Mercury", # Comstar command level II
      "Turkina", "Kingfisher", "Crossbow", "Cauldron-Born", "Nobori-nin", # Clan heavy battle star
      "Behemoth", "Supernova", "Marauder IIC", "Warhammer IIC", "Hunchback IIC", # Clan heavy star
      "Masakari", "Nova Cat", "Cougar", "Uller", "Dasher", # Clan fire star
      "Night Gyr", "Linebacker", "Black Lanner", "Battle Cobra", "Hankyu", # Clan support star
      "Kodiak", "Fire Falcon", "Hellion", "Baboon", "Pack Hunter", # Clan ad-hoc star
      "Banshee", "Grasshopper", "Centurion", "Hatchetman", # Inner Sphere heavy lance
      "Blackjack", "Panther", "Wolfhound", "Jenner", # Inner Sphere striker lance
      "Stalker", "Longbow", "Zeus", "Trebuchet", # Inner Sphere fire lance
      "Nightstar", "Cataphract", "Axman", "Bushwacker", # Inner Sphere heavy battle lance
      "Victor", "Enforcer", "Hunchback", "Raven", # Inner Sphere urban lance
      "Cyclops", "Thug", "Dragon", "Spider", # Inner Sphere support lance
      "Crockett", "Flashman", "Guillotine", "Lancelot", "Crab", "Mongoose", # Comstar battle level II
      "Behemoth", "Supernova", "Marauder IIC", "Warhammer IIC", "Hunchback IIC", # Clan Heavy Star
      "Turkina", "Kingfisher", "Cauldron-Born", "Crossbow", "Nobori-nin", # Clan Heavy Battle Star
    ]
    mechs_in_clan_invasion_wave2.each do |chassis_name|
      chassis = MechChassis.find_by(name: chassis_name)
      if chassis.nil?
        raise "Couldn't find mech chassis named \"#{chassis_name}\"."
      end

      if mechs_bought_in_clan_invasion_kickstarter.include?(chassis.name)
        chassis.plastic_model_owned = true
      end
      chassis.plastic_model_release_date = Date.new(2020, 12, 1)
      chassis.save!
    end

    mechs_in_wolfs_dragoons_assault_force_pack = ["Annihilator"]
    mechs_in_wolfs_dragoons_assault_force_pack.each do |chassis_name|
      chassis = MechChassis.find_by(name: chassis_name)
      if chassis.nil?
        raise "Couldn't find mech chassis named \"#{chassis_name}\"."
      end

      chassis.plastic_model_release_date = Date.new(2021, 11, 01)
      chassis.save!
    end
  end
end
